package com.example.customermanageadmin.realm_model

import com.example.customermanageadmin.main.customers.livedata.Customer
import com.example.customermanageadmin.main.home.livedata.FilterData
import io.realm.Case
import io.realm.Realm
import io.realm.RealmList
import javax.inject.Inject

class CustomerRealmRepository : CustomerRepository {
    lateinit var realm: Realm

    @Inject
    constructor(realm: Realm) {
        this.realm = realm
    }

    constructor() {}

    override fun getUserInfo(): Customer? {
        initRealm()
        return realm.where(Customer::class.java).findFirst()
    }

    override fun closeRealm() {
        if (realm != null && !realm.isClosed) {
            realm.close()
        }
    }

    private fun initRealm() {
        if (realm == null || realm.isClosed) {
            realm = Realm.getDefaultInstance()
        }
    }

    override fun insertItem(savedItem: Customer, id: String) {
        initRealm()
        realm.executeTransaction { realm ->
            savedItem.setItemId(id)
            realm.insert(savedItem)
        }
    }

    override fun insertOrUpdate(savedItem: List<Customer>) {
        initRealm()
        val customerList = RealmList<Customer>()
        customerList.addAll(savedItem)
        realm.executeTransaction { realm -> realm.insertOrUpdate(customerList) }
    }

    override fun insertOrUpdate(savedItem: Customer) {
        initRealm()
        realm.executeTransaction { realm -> realm.insertOrUpdate(savedItem) }
    }

    override fun countTotalItems(): Int {
        initRealm()
        return realm.where(Customer::class.java).count().toInt()
    }

    override fun countTotalAmount(): Float {
        initRealm()
        return realm.where(Customer::class.java).sum("amount").toFloat()
    }

    override fun countTotalItemsFilter(filterData: FilterData): Int {
        initRealm()
        return if (filterData.from_date != "" && filterData.paymentMode != 0) {
            realm.where(Customer::class.java)

                .contains("name", filterData.name, Case.INSENSITIVE).or()
                .contains("email", filterData.name, Case.INSENSITIVE).or()
                .contains("phone", filterData.name, Case.INSENSITIVE)

                .findAll()
                .where()

                .equalTo("payment_mode", filterData.paymentMode.toString())

                .greaterThanOrEqualTo("created", filterData.fromDate).and()
                .lessThan("created", filterData.toDate)

                .findAll().count().toInt()
        } else if (filterData.paymentMode != 0 && filterData.from_date == "") {
            realm.where(Customer::class.java)

                .contains("name", filterData.name, Case.INSENSITIVE).or()
                .contains("email", filterData.name, Case.INSENSITIVE).or()
                .contains("phone", filterData.name, Case.INSENSITIVE)

                .findAll()
                .where()

                .equalTo("payment_mode", filterData.paymentMode.toString())
                .findAll().count().toInt()

        } else if (filterData.paymentMode == 0 && filterData.from_date == "") {
            realm.where(Customer::class.java)

                .contains("name", filterData.name, Case.INSENSITIVE).or()
                .contains("email", filterData.name, Case.INSENSITIVE).or()
                .contains("phone", filterData.name, Case.INSENSITIVE).and()

                .findAll().count().toInt()

        } else if (filterData.paymentMode == 0 && filterData.from_date != "") {
            realm.where(Customer::class.java)

                .contains("name", filterData.name, Case.INSENSITIVE).or()
                .contains("email", filterData.name, Case.INSENSITIVE).or()
                .contains("phone", filterData.name, Case.INSENSITIVE).and()

                .findAll()
                .where()

                .greaterThanOrEqualTo("created", filterData.fromDate).and()
                .lessThan("created", filterData.toDate)

                .findAll().count().toInt()
        } else {
            realm.where(Customer::class.java)

                .contains("name", filterData.name, Case.INSENSITIVE).or()
                .contains("email", filterData.name, Case.INSENSITIVE).or()
                .contains("phone", filterData.name, Case.INSENSITIVE).and()

                .findAll().count().toInt()
        }
//        return realm.where(Customer::class.java).count().toInt()
    }

    override fun countTotalAmountFilter(filterData: FilterData): Float {
        initRealm()
        return if (filterData.from_date != "" && filterData.paymentMode != 0) {
            realm.where(Customer::class.java)

                .contains("name", filterData.name, Case.INSENSITIVE).or()
                .contains("email", filterData.name, Case.INSENSITIVE).or()
                .contains("phone", filterData.name, Case.INSENSITIVE)

                .findAll()
                .where()

                .equalTo("payment_mode", filterData.paymentMode.toString())

                .greaterThanOrEqualTo("created", filterData.fromDate).and()
                .lessThan("created", filterData.toDate)

                .findAll().sum("amount").toFloat()
        } else if (filterData.paymentMode != 0 && filterData.from_date == "") {
            realm.where(Customer::class.java)

                .contains("name", filterData.name, Case.INSENSITIVE).or()
                .contains("email", filterData.name, Case.INSENSITIVE).or()
                .contains("phone", filterData.name, Case.INSENSITIVE)

                .findAll()
                .where()

                .equalTo("payment_mode", filterData.paymentMode.toString())
                .findAll().sum("amount").toFloat()

        } else if (filterData.paymentMode == 0 && filterData.from_date == "") {
            realm.where(Customer::class.java)

                .contains("name", filterData.name, Case.INSENSITIVE).or()
                .contains("email", filterData.name, Case.INSENSITIVE).or()
                .contains("phone", filterData.name, Case.INSENSITIVE).and()

                .findAll().sum("amount").toFloat()

        } else if (filterData.paymentMode == 0 && filterData.from_date != "") {
            realm.where(Customer::class.java)

                .contains("name", filterData.name, Case.INSENSITIVE).or()
                .contains("email", filterData.name, Case.INSENSITIVE).or()
                .contains("phone", filterData.name, Case.INSENSITIVE).and()

                .findAll()
                .where()

                .greaterThanOrEqualTo("created", filterData.fromDate).and()
                .lessThan("created", filterData.toDate)

                .findAll().sum("amount").toFloat()
        } else {
            realm.where(Customer::class.java)

                .contains("name", filterData.name, Case.INSENSITIVE).or()
                .contains("email", filterData.name, Case.INSENSITIVE).or()
                .contains("phone", filterData.name, Case.INSENSITIVE).and()

                .findAll().sum("amount").toFloat()
        }



//        return realm.where(Customer::class.java).sum("amount").toFloat()
    }

    override fun findAll(): List<Customer> {
        initRealm()
        return realm.where(Customer::class.java)
            .findAll()
    }

    override fun findWithFilter(filterData: FilterData): List<Customer> {
        initRealm()
        return if (filterData.from_date != "" && filterData.paymentMode != 0) {
            realm.where(Customer::class.java)

                .contains("name", filterData.name, Case.INSENSITIVE).or()
                .contains("email", filterData.name, Case.INSENSITIVE).or()
                .contains("phone", filterData.name, Case.INSENSITIVE)

                .findAll()
                .where()

                .equalTo("payment_mode", filterData.paymentMode.toString())

                .greaterThanOrEqualTo("created", filterData.fromDate).and()
                .lessThan("created", filterData.toDate)

                .findAll()
        } else if (filterData.paymentMode != 0 && filterData.from_date == "") {
            realm.where(Customer::class.java)

                .contains("name", filterData.name, Case.INSENSITIVE).or()
                .contains("email", filterData.name, Case.INSENSITIVE).or()
                .contains("phone", filterData.name, Case.INSENSITIVE)

                .findAll()
                .where()

                .equalTo("payment_mode", filterData.paymentMode.toString())
                .findAll()

        } else if (filterData.paymentMode == 0 && filterData.from_date == "") {
            realm.where(Customer::class.java)

                .contains("name", filterData.name, Case.INSENSITIVE).or()
                .contains("email", filterData.name, Case.INSENSITIVE).or()
                .contains("phone", filterData.name, Case.INSENSITIVE).and()

                .findAll()

        } else if (filterData.paymentMode == 0 && filterData.from_date != "") {
            realm.where(Customer::class.java)

                .contains("name", filterData.name, Case.INSENSITIVE).or()
                .contains("email", filterData.name, Case.INSENSITIVE).or()
                .contains("phone", filterData.name, Case.INSENSITIVE).and()

                .findAll()
                .where()

                .greaterThanOrEqualTo("created", filterData.fromDate).and()
                .lessThan("created", filterData.toDate)

                .findAll()
        } else {
            realm.where(Customer::class.java)

                .contains("name", filterData.name, Case.INSENSITIVE).or()
                .contains("email", filterData.name, Case.INSENSITIVE).or()
                .contains("phone", filterData.name, Case.INSENSITIVE).and()

                .findAll()
        }


    }

    override fun deleteAllItems() {
        initRealm()
        realm.executeTransaction { realm -> realm.delete(Customer::class.java) }
    }

}

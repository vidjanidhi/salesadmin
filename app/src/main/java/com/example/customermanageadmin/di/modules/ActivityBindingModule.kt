package com.example.customermanageadmin.di.modules

import com.example.customermanageadmin.main.splash.SplashActivity
import com.example.customermanageadmin.main.customers.CustomersListActivity
import com.example.customermanageadmin.main.customers.livedata.CustomerListActivityModule
import com.example.customermanageadmin.main.engineer_list.EngineerList
import com.example.customermanageadmin.main.home.MainActivity
import com.example.customermanageadmin.main.home.livedata.MainActivityModule
import com.example.customermanageadmin.main.location.LocationActivity
import com.example.customermanageadmin.main.login.LoginActivity
import com.example.customermanageadmin.main.logout.LogoutActivity
import com.example.customermanageadmin.main.register.AddEngineerActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector
import deliverr.deliverrconsumer.di.scopes.ActivityScope


@Module
abstract class ActivityBindingModule {
    @ActivityScope
    @ContributesAndroidInjector(modules = [])
    internal abstract fun bindHomeActivity(): MainActivityModule

    @ActivityScope
    @ContributesAndroidInjector(modules = [])
    internal abstract fun bindMainActivity(): MainActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [])
    internal abstract fun bindLoginActivity(): LoginActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [])
    internal abstract fun bindSplashActivity(): SplashActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [])
    internal abstract fun bindAddEngineerActivity(): AddEngineerActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [CustomerListActivityModule::class])
    internal abstract fun bindSBrandItemsActivity(): CustomersListActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [])
    internal abstract fun bindEngineerListActivity(): EngineerList

    @ActivityScope
    @ContributesAndroidInjector(modules = [])
    internal abstract fun bindLogoutActivity(): LogoutActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [])
    internal abstract fun bindLcationActivity(): LocationActivity

}

package com.example.customermanageadmin.di.location;

import android.app.Activity;

import com.example.customermanageadmin.application.MyLifecycleCallbackAdapter;
import com.example.customermanageadmin.application.utils.PlayServicesUtils;
import com.example.customermanageadmin.main.StandardActivity;
import com.example.customermanageadmin.prefs.SharedPrefsService;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

import javax.inject.Inject;


public class LocationManagerLifecycleHandler extends MyLifecycleCallbackAdapter {
    public static class ConnectionFailedListener implements GoogleApiClient.OnConnectionFailedListener {
        private Activity activity;

        public ConnectionFailedListener(Activity activity) {
            this.activity = activity;
        }

        @Override
        public void onConnectionFailed(ConnectionResult connectionResult) {
            PlayServicesUtils.onConnectionFailed(connectionResult, activity);
        }
    }

    private boolean locationStopRequested;
    @Inject
    LocationManager lm;
    @Inject
    SharedPrefsService deliverrSharedPrefs;

    @Inject
    public LocationManagerLifecycleHandler() {
    }

    @Override
    public void activityPaused(StandardActivity activity) {
        lm.startDisconnectTimer();
        locationStopRequested = true;
    }

    @Override
    public void activityStarted(StandardActivity activity) {
        locationStopRequested = false;

        ConnectionFailedListener listener = new ConnectionFailedListener(activity);
        lm.connectClient(listener);
    }

    @Override
    public void activityStopped(StandardActivity activity) {
        if (locationStopRequested) {
            lm.startDisconnectTimer();
        }
    }

    @Override
    public void activityDestroyed(StandardActivity activity) {
    }
}

package com.example.customermanageadmin.di.conductor;

import android.app.Activity;

import com.bluelinelabs.conductor.Controller;

import dagger.android.AndroidInjector;
import dagger.internal.Preconditions;

public final class ConductorInjection {

    private ConductorInjection() {
    }

    /**
     * Injects the {@link Controller}
     */
    public static void inject(Controller controller) {
        Preconditions.checkNotNull(controller, "controller");
        HasConductorInjector hasDispatchingControllerInjector = findHasControllerInjector(controller);
        AndroidInjector<Controller> controllerInjector = hasDispatchingControllerInjector.controllerInjector();
        Preconditions.checkNotNull(controllerInjector, "%s.controllerInjector() returned null",
                hasDispatchingControllerInjector.getClass().getCanonicalName());
        controllerInjector.inject(controller);
    }

    private static HasConductorInjector findHasControllerInjector(Controller controller) {
        Controller parentController = controller;

        do {
            if ((parentController = parentController.getParentController()) == null) {
                Activity activity = controller.getActivity();
                if (activity instanceof HasConductorInjector) {
                    return (HasConductorInjector) activity;
                }

                if (activity.getApplication() instanceof HasConductorInjector) {
                    return (HasConductorInjector) activity.getApplication();
                }

                throw new IllegalArgumentException(
                        String.format("No injector was found for %s", controller.getClass().getCanonicalName()));
            }
        } while (!(parentController instanceof HasConductorInjector));

        return (HasConductorInjector) parentController;
    }
}
package com.example.customermanageadmin.di.modules

import android.app.Application
import com.example.customermanageadmin.prefs.SharedPrefs
import com.example.customermanageadmin.prefs.SharedPrefsService
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class SharedPrefsModule {
    @Provides
    @Singleton
    fun provideDeliverrSharedPrefsService(application: Application): SharedPrefsService {
        return SharedPrefs(application)
    }
}
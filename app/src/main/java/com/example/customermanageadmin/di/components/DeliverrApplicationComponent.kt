package com.example.customermanageadmin.di.components

import android.app.Application
import com.example.customermanageadmi.application.location.LocationManagerModule
import com.example.customermanageadmin.application.MyApplication
import com.example.customermanageadmin.di.conductor.ConductorInjectionModule
import com.example.customermanageadmin.di.modules.*
import com.example.customermanageadmin.di.views.ViewInjectionModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule

import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        ApplicationModule::class,
        SharedPrefsModule::class,
        AndroidSupportInjectionModule::class,
        ConductorInjectionModule::class,
        ViewInjectionModule::class,
        RealmModule::class,
        ActivityBindingModule::class,
        ConductorBindingModule::class,
        ViewBindingModule::class
    ]
)
interface DeliverrApplicationComponent : AndroidInjector<MyApplication> {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): DeliverrApplicationComponent
    }
}

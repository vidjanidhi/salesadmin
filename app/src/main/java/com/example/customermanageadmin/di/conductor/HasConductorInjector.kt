package com.example.customermanageadmin.di.conductor

import com.bluelinelabs.conductor.Controller
import dagger.android.DispatchingAndroidInjector

interface HasConductorInjector {
    fun controllerInjector(): DispatchingAndroidInjector<Controller>
}
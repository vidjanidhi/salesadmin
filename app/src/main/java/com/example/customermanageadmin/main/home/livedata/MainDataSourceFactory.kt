package deliverr.deliverrconsumer.screens.add_item.store_categories_grid_view.brand_items_screen

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.example.customermanageadmin.main.customers.livedata.Customer
import com.example.customermanageadmin.main.home.livedata.FilterData
import com.example.customermanageadmin.main.login.rx.RetrofitRxWrapper
import com.example.customermanageadmin.realm_model.CustomerRealmRepository
import com.example.customermanageadmin.realm_model.CustomerRepository

class MainDataSourceFactory(
    private val filterData: FilterData,
    private val wrapper: RetrofitRxWrapper,
    private val isNetworkAvailable: Boolean
) : DataSource.Factory<Int, Customer>() {
    val mainDataSourceLiveData: MutableLiveData<MainDataSource>
    private lateinit var brandItemsDataSource: MainDataSource

    init {
        this.mainDataSourceLiveData = MutableLiveData()
    }

    override fun create(): DataSource<Int, Customer> {
        this.brandItemsDataSource = MainDataSource(
           filterData,
            this.wrapper,isNetworkAvailable
        )
        this.mainDataSourceLiveData.postValue(this.brandItemsDataSource)
        return this.brandItemsDataSource
    }
}

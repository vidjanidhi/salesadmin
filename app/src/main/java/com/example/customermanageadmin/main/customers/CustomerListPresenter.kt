package deliverr.deliverrconsumer.referral.account_status

import com.example.customermanageadmin.R
import com.example.customermanageadmin.application.utils.NetworkState
import com.example.customermanageadmin.application.utils.NetworkStatus
import com.example.customermanageadmin.base.BaseServiceRxWrapper
import com.example.customermanageadmin.main.customers.CustomerAnalyticResponse
import com.example.customermanageadmin.main.customers.CustomerListViewModel
import com.example.customermanageadmin.main.login.rx.RetrofitRxWrapper
import com.example.customermanageadmin.realm_model.CustomerRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import java.net.UnknownHostException

class CustomerListPresenter(
    private val viewDelegate: CustomerListViewDelegate,
    private val wrapper: RetrofitRxWrapper,
    private val customerRepository: CustomerRepository,
    private val isNetworkAvailable: Boolean
) {
    val viewModel: CustomerListViewModel by lazy {
        this.viewDelegate.getViewModel(CustomerListViewModel::class.java)
    }
    private var disposable: Disposable? = null

    fun handleInitialLoadingNetworkState(networkState: NetworkState) {
        when (networkState.status) {
            NetworkStatus.RUNNING -> this.viewDelegate.showProgressBar()
            NetworkStatus.SUCCESS -> {
                this.viewDelegate.hideProgressBar()
            }
            NetworkStatus.FAILED -> {
                this.viewDelegate.hideProgressBar()
                if (networkState.message.isNotBlank()) {
                    this.viewDelegate.showMessage(networkState.message)
                } else {
                    this.viewDelegate.showMessage("Something Went Wrong")
                }
            }
        }
    }

    fun getItems() {
        viewModel.initItems(wrapper, customerRepository, isNetworkAvailable)
        viewModel.customerListResponse?.let { viewDelegate.setAdapter(it) };
    }

    fun getDetail() {
        if (viewDelegate.isConnectedToInternet()) {
            viewDelegate.showProgressBar()
            getAnalyticsDetails()
        } else {
            getAnalyticsDetailsFromRealm()
            viewDelegate.hideProgressBar()
            viewDelegate.showMessage(R.string.no_internet_connection_message)
        }
    }

    private fun getAnalyticsDetailsFromRealm() {

        var customerAnalyticResponse = CustomerAnalyticResponse()
        customerAnalyticResponse.total = customerRepository.countTotalItems().toString()
        customerAnalyticResponse.total_amount = customerRepository.countTotalAmount().toString()
        viewModel.customerListResponse = customerAnalyticResponse
        success()
    }

    fun getAnalyticsDetails() {
        viewDelegate.hideProgressBar()
        if (viewModel.customerListSingle == null) {
            viewModel.customerListSingle =
                wrapper.getBrandItemListDetail()
        }
        disposable?.dispose()

        disposable = this.viewModel.customerListSingle?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribeWith(object : DisposableSingleObserver<CustomerAnalyticResponse>() {
                override fun onSuccess(customerListResponse: CustomerAnalyticResponse) {

                    viewModel.customerListResponse = customerListResponse
                    viewModel.customerListSingle = null
                    if (customerListResponse.isSuccess == true) {
                        success()
                    } else {
                        customerListResponse.message?.let { failed(it) }
                    }
                }

                override fun onError(e: Throwable) {
                    viewModel.customerListSingle = null
                    var message: String
                    if (e is UnknownHostException) {
                        message = BaseServiceRxWrapper.NO_INTERNET_CONNECTION
                    } else {
                        message = e.message.toString()
                    }
                    failed(message)
                    dispose()
                }
            })
    }

    fun success() {
        getItems()
    }

    fun failed(msg: String) {
        viewDelegate.showMessage(msg)
    }


}

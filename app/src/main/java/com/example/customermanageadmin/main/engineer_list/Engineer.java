
package com.example.customermanageadmin.main.engineer_list;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import io.realm.RealmModel;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class Engineer implements RealmModel, Parcelable {

    @SerializedName("id")
    private String id;
    @SerializedName("name")
    private String name;
    @SerializedName("email")
    private String email;
    @SerializedName("phone")
    private String phone;
    @SerializedName("address")
    private String address;
    @SerializedName("password")
    private String password;

    private  Boolean isEdit=false;
    private  Boolean isAdd=false;

    public Engineer(){

    }

    public Engineer(Parcel in) {
        id = in.readString();
        name = in.readString();
        email = in.readString();
        phone = in.readString();
        address = in.readString();
        password = in.readString();
        byte tmpIsEdit = in.readByte();
        isEdit = tmpIsEdit == 0 ? null : tmpIsEdit == 1;
        byte tmpIsAdd = in.readByte();
        isAdd = tmpIsAdd == 0 ? null : tmpIsAdd == 1;
    }

    public static final Creator<Engineer> CREATOR = new Creator<Engineer>() {
        @Override
        public Engineer createFromParcel(Parcel in) {
            return new Engineer(in);
        }

        @Override
        public Engineer[] newArray(int size) {
            return new Engineer[size];
        }
    };

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getAdd() {
        return isAdd;
    }

    public void setAdd(Boolean add) {
        isAdd = add;
    }

    public Boolean getEdit() {
        return isEdit;
    }

    public void setEdit(Boolean edit) {
        isEdit = edit;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(email);
        dest.writeString(phone);
        dest.writeString(address);
        dest.writeString(password);
        dest.writeByte((byte) (isEdit == null ? 0 : isEdit ? 1 : 2));
        dest.writeByte((byte) (isAdd == null ? 0 : isAdd ? 1 : 2));
    }
}

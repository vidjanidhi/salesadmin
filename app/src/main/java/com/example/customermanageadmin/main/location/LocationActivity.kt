package com.example.customermanageadmin.main.location

import android.content.Context
import android.content.Intent
import android.location.Location
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.SeekBar
import com.example.customermanageadmin.main.StandardActivity
import com.example.customermanageadmin.main.login.rx.RetrofitRxWrapper
import com.example.customermanageadmin.models.PaymentMode
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.activity_location.*


class LocationActivity : StandardActivity(), OnMapReadyCallback, LocationDelegate {
    private var presenter: LocationPresenter? = null
    var googleMap: GoogleMap? = null
    private var mFusedLocationClient: FusedLocationProviderClient? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.example.customermanageadmin.R.layout.activity_location)

        setSupportActionBar(toolbar_eng)

        if (this.supportActionBar != null) {
            this.supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setTitle("Customers Locations")
        }

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        presenter = LocationPresenter(
            this,
            RetrofitRxWrapper()
        )

        val mapFragment = supportFragmentManager
            .findFragmentById(com.example.customermanageadmin.R.id.map) as SupportMapFragment?
        if (mapFragment != null) {
            mapFragment.getMapAsync(this)
        }


        seekbar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            internal var progressChangedValue = 0

            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                progressChangedValue = progress
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {
                // TODO Auto-generated method stub
            }

            override fun onStopTrackingTouch(seekBar: SeekBar) {
                presenter!!.changeDistance(seekBar.progress)
            }
        })

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        // Add a marker in Sydney, Australia,
        // and move the map's camera to the same location.
        this.googleMap = googleMap
        googleMap.setMyLocationEnabled(true)

        googleMap.animateCamera(CameraUpdateFactory.zoomTo(3.0f));
        mFusedLocationClient?.lastLocation?.addOnSuccessListener { location: Location? ->
            // Got last known location. In some rare situations this can be null.
            presenter?.storeLocation(location)
            presenter?.initialization()
        }
    }

    override fun setMarkers(list: List<CustomerLocation>) {
        if (googleMap != null) {
            googleMap?.mapType = GoogleMap.MAP_TYPE_TERRAIN
            for (customerLocation in list) {
                val pin = LatLng(customerLocation.lat.toDouble(), customerLocation.lng.toDouble())
                val markerOptions = MarkerOptions().position(pin)
                    .title(customerLocation.name)
                    .snippet(
                        "Email : " + customerLocation.email + "\nAmount : " + "₹ "+customerLocation.amount + "\nPayment Mode : " + PaymentMode.getStringPaymentMode(
                            customerLocation.payment_mode.toInt()
                        )
                    )
                val marker = googleMap!!.addMarker(markerOptions)
                marker.showInfoWindow()

            }
            googleMap!!.moveCamera(
                CameraUpdateFactory.newLatLng(
                    presenter?.getcurrentLng()?.let { LatLng(presenter?.getcurrentLat()!!, it) }
                )
            )

            googleMap!!.setOnMarkerClickListener(onMarkerClickedListener);

        }
    }

    private val onMarkerClickedListener =
        GoogleMap.OnMarkerClickListener { marker ->
            if (marker.isInfoWindowShown) {
                description.visibility = View.GONE
                marker.hideInfoWindow()
            } else {
                description.visibility = View.VISIBLE
                description.setText("Name : " + marker.title + "\n" + marker.snippet)
                marker.showInfoWindow()
            }
            true
        }

    override fun clearMap() {
        if (googleMap != null) {
            googleMap!!.clear()
        }
    }

    override fun setMaxDistance(distance: Long) {
//        seekbar.max = distance.toInt()
    }

    override fun setMin(distance: Long) {
        min_distance.text = "0%"
    }

    override fun setMax(distance: Long) {
        max_distance.text = "100%"
    }

    override fun getSeekPosition(): Int {
        return seekbar.progress
    }

    companion object {
        fun getLauncherIntent(context: Context): Intent {
            return Intent(context, LocationActivity::class.java)
        }

        @JvmStatic
        fun launch(context: Context) {
            context.startActivity(getLauncherIntent(context))
        }
    }

}



package com.example.customermanageadmin.main.customers.livedata

import android.view.View
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.example.customermanageadmin.R
import com.example.customermanageadmin.main.customers.CustomerAnalyticResponse
import com.example.customermanageadmin.main.home.livedata.FilterData
import deliverr.deliverrconsumer.utils.roundOffDecimal

class CustomerHeaderViewHolder(
    rootView: View
) : RecyclerView.ViewHolder(rootView) {
    private val customer: AppCompatTextView = rootView.findViewById(R.id.customer)
    private val amount: AppCompatTextView = rootView.findViewById(R.id.amount)


    fun bind(item: CustomerAnalyticResponse) {

        amount.setText(
            if (item.total_amount == null) {
                "00"
            } else {
                "₹ " + roundOffDecimal(item.total_amount.toFloat())
            }
        )
        customer.setText(
            if (item.total == null) {
                "00"
            } else {
                item.total
            }
        )

    }


}

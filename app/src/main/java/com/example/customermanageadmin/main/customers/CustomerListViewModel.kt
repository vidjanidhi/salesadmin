package com.example.customermanageadmin.main.customers

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.example.customermanageadmin.application.utils.NetworkState
import com.example.customermanageadmin.main.customers.livedata.Customer
import com.example.customermanageadmin.main.login.rx.RetrofitRxWrapper
import com.example.customermanageadmin.realm_model.CustomerRepository
import deliverr.deliverrconsumer.screens.add_item.store_categories_grid_view.brand_items_screen.CustomerListDataSource
import deliverr.deliverrconsumer.screens.add_item.store_categories_grid_view.brand_items_screen.CustomerListDataSourceFactory
import deliverr.deliverrconsumer.screens.add_item.store_categories_grid_view.brand_items_screen.FUTURE_PAGE_SIZE
import deliverr.deliverrconsumer.screens.add_item.store_categories_grid_view.brand_items_screen.INITIAL_PAGE_SIZE
import io.reactivex.Single

class CustomerListViewModel : ViewModel() {

    var customerListSingle: Single<CustomerAnalyticResponse>? = null
    var customerListResponse: CustomerAnalyticResponse? = null

    lateinit var initialLoading: LiveData<NetworkState>
    lateinit var networkState: LiveData<NetworkState>
    lateinit var menuItemsLiveData: LiveData<PagedList<Customer>>

    lateinit var brandItemsDataSource: LiveData<CustomerListDataSource>

    fun retry() {
        this.brandItemsDataSource.value!!.retryLoading()
    }

    fun initItems(
        wrapper: RetrofitRxWrapper,
        customerRepository: CustomerRepository,
        isNetworkAvailable: Boolean
    ) {
        val brandItemsDataSourceFactory = CustomerListDataSourceFactory(
            wrapper,
            customerRepository, isNetworkAvailable
        )
        this.brandItemsDataSource = brandItemsDataSourceFactory.brandItemsDataSourceLiveData

        this.initialLoading = Transformations.switchMap(
            brandItemsDataSourceFactory.brandItemsDataSourceLiveData
        ) { it.initialLoading }
        this.networkState = Transformations.switchMap(
            brandItemsDataSourceFactory.brandItemsDataSourceLiveData
        ) { it.networkState }

        val pagedListConfig = PagedList.Config.Builder()
            .setEnablePlaceholders(false)
            .setInitialLoadSizeHint(INITIAL_PAGE_SIZE)
            .setPageSize(FUTURE_PAGE_SIZE)
            .setPrefetchDistance(FUTURE_PAGE_SIZE)
            .build()

        this.menuItemsLiveData = LivePagedListBuilder(brandItemsDataSourceFactory, pagedListConfig)
            .build()
    }
}

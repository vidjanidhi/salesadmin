package com.example.customermanageadmin.main.splash

import com.example.customermanageadmin.main.customers.livedata.CustomerListResponse
import com.example.customermanageadmin.main.engineer_list.Engineer
import com.example.customermanageadmin.main.engineer_list.EngineerListResponse
import com.example.customermanageadmin.main.login.rx.RetrofitRxWrapper
import com.example.customermanageadmin.main.register.rx.AddEngineerResponse
import com.example.customermanageadmin.realm_model.CustomerRepository
import com.example.customermanageadmin.realm_model.EngineerRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers

class SplashActivityPresenter(
    private val delegate: SplashDelegate,
    private val wrapper: RetrofitRxWrapper,
    private val customerRepository: CustomerRepository,
    private val engineerRepository: EngineerRepository
) {

    private var disposable: Disposable? = null

    private val viewModel: SplashViewModel by lazy {
        this.delegate.getViewModel(SplashViewModel::class.java)
    }

    fun getCustomerList() {
        if (viewModel.customerListSingle == null) {
            viewModel.customerListSingle =
                wrapper.getBrandItemListCall()
        }
        disposable?.dispose()

        disposable = this.viewModel.customerListSingle?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribeWith(object : DisposableSingleObserver<CustomerListResponse>() {
                override fun onSuccess(customerListResponse: CustomerListResponse) {

                    viewModel.customerListResponse = customerListResponse
                    viewModel.customerListSingle = null
                    if (customerListResponse.isSuccess == true) {
                        storeToLocal()
                    } else {
                        customerListResponse.message?.let { }
                    }
                }

                override fun onError(e: Throwable) {
                    viewModel.customerListSingle = null
                    dispose()
                }
            })
    }

    fun storeToLocal() {
        customerRepository.deleteAllItems()
        var count = customerRepository.countTotalItems()
        viewModel.customerListResponse?.payload?.let { customerRepository.insertOrUpdate(it) }
        var count1 = customerRepository.countTotalItems()
        sendAddedEngineers()

//        delegate.openNext()
    }

    private fun sendAddedEngineers() {
        var engineers = engineerRepository.findAdds()
        if (engineers.size > 0) {
            addEngineerWrapper(engineers[0])
        } else {
            sendEditedEngineers()
        }

    }

    fun addEngineerWrapper(engineer: Engineer) {

        if (viewModel.addResponseSingle == null) {
            viewModel.addResponseSingle =
                wrapper.addEngineer(
                    engineer.getName(),
                    engineer.getEmail(),
                    engineer.getPhone(),
                    engineer.getAddress(),
                    engineer.getPassword()
                )
        }
        disposable?.dispose()

        disposable = this.viewModel.addResponseSingle?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribeWith(object : DisposableSingleObserver<AddEngineerResponse>() {
                override fun onSuccess(loginResponse: AddEngineerResponse) {

                    viewModel.addResponse = loginResponse
                    viewModel.addResponseSingle = null
                    if (loginResponse.isSuccess == true) {
                        deleteAddedEngineer(engineer.email)
                        sendAddedEngineers()
                    } else {
                        deleteAddedEngineer(engineer.email)
                        sendAddedEngineers()
                    }
                }

                override fun onError(e: Throwable) {
                    viewModel.addResponseSingle = null
                    dispose()
                }
            })
    }

    private fun deleteAddedEngineer(email: String) {
        engineerRepository.deleteAddedItem(email)
    }

    private fun sendEditedEngineers() {
        var engineers = engineerRepository.findEdits()
        if (engineers.size > 0) {
            editEngineerWrapper(engineers[0])
        } else {
            getEngineers()
        }
    }

    fun editEngineerWrapper(engineer: Engineer) {

        if (viewModel.editResponseSingle == null) {
            viewModel.editResponseSingle =
                wrapper.editEngineer(
                    engineer?.id,
                    engineer.getName(),
                    engineer.getEmail(),
                    engineer.getPhone(),
                    engineer.getAddress()
                )
        }
        disposable?.dispose()

        disposable = this.viewModel.editResponseSingle?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribeWith(object : DisposableSingleObserver<AddEngineerResponse>() {
                override fun onSuccess(editResponse: AddEngineerResponse) {

                    viewModel.editResponse = editResponse
                    viewModel.editResponseSingle = null
                    if (editResponse.isSuccess == true) {
                        deleteEditedEngineer(engineer.id)
                    } else {
                    }
                }

                override fun onError(e: Throwable) {
                    viewModel.editResponseSingle = null

                    dispose()
                }
            })
    }

    private fun deleteEditedEngineer(id: String) {
        engineerRepository.deleteEditedItem(id)
        sendEditedEngineers()
    }


    fun getEngineers() {

        if (viewModel.engineerResponseSingle == null) {
            viewModel.engineerResponseSingle =
                wrapper.getEngineers()
        }
        disposable?.dispose()

        disposable = this.viewModel.engineerResponseSingle?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribeWith(object : DisposableSingleObserver<EngineerListResponse>() {
                override fun onSuccess(loginResponse: EngineerListResponse) {

                    viewModel.engineerListResponse = loginResponse
                    viewModel.engineerResponseSingle = null
                    if (loginResponse.isSuccess == true) {
                        success()

                    } else {

                    }
                }

                override fun onError(e: Throwable) {
                    viewModel.engineerResponseSingle = null

                    dispose()
                }
            })
    }

    private fun success() {
        engineerRepository.deleteAllGetItems()
        var count = engineerRepository.countTotalItems()
        viewModel.engineerListResponse?.payload?.let { engineerRepository.insertOrUpdate(it) }
        var count1 = engineerRepository.countTotalItems()
        delegate.openNext()
    }

}

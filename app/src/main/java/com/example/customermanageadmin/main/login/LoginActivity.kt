package com.example.customermanageadmin.main.login

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.AppCompatImageView
import com.example.customermanageadmin.R
import com.example.customermanageadmin.main.StandardActivity
import com.example.customermanageadmin.main.home.MainActivity
import com.example.customermanageadmin.main.login.rx.RetrofitRxWrapper
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import deliverr.deliverrconsumer.utils.isValidEmail
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : StandardActivity(), LoginDelegate {
    private val KEY_EMAIL = "KEY_EMAIL"
    private val KEY_PASSWORD = "KEY_PASSWORD"
    lateinit var emailInputLayout: TextInputLayout
    lateinit var passwordInputLayout: TextInputLayout

    lateinit var email: TextInputEditText
    lateinit var password: TextInputEditText
    lateinit var backButton: AppCompatImageView
    private var loginPresenter: LoginPresenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        bindViews()
        initializeView(savedInstanceState)
    }

    fun bindViews() {
        emailInputLayout = findViewById(R.id.emailInputLayout)
        passwordInputLayout = findViewById(R.id.passwordInputLayout)
        email = findViewById(R.id.email)
        password = findViewById(R.id.password)
        backButton = findViewById(R.id.backButton)
        backButton.setOnClickListener({ view1 -> clickBackButton() })
        loginButton.setOnClickListener({ view1 -> clickLoginButton() })
    }

    fun initializeView(savedViewState: Bundle?) {
        if (loginPresenter == null) {
            this.loginPresenter = LoginPresenter(
                this,
                RetrofitRxWrapper()
            )
        }
        email.addTextChangedListener(loginPresenter)
        password.addTextChangedListener(loginPresenter)

        restoreViewState(savedViewState)
    }

    fun clickLoginButton() {
        if (isUiValid()) {
            loginPresenter?.login()
        }
    }


    fun clickBackButton() {
        finishActivity()
    }


    private fun restoreViewState(savedViewState: Bundle?) {
        if (savedViewState != null) {
            email.setText(savedViewState.getString(KEY_EMAIL))
            password.setText(savedViewState.getString(KEY_PASSWORD))

        }
    }

    private fun isUiValid(): Boolean {
        var isUiValid = true

        if (email.text.toString().trim { it <= ' ' }.isEmpty()) {
            emailInputLayout.requestFocus()
            emailInputLayout.isErrorEnabled = true
            emailInputLayout.error = getString(R.string.error_email)
            isUiValid = false
            return isUiValid
        } else {
            emailInputLayout.isErrorEnabled = false
        }

        if (!isValidEmail(email.text.toString())) {
            emailInputLayout.requestFocus()
            emailInputLayout.isErrorEnabled = true
            emailInputLayout.error = getString(R.string.error_valid_email)
            isUiValid = false
            return isUiValid
        } else {
            emailInputLayout.isErrorEnabled = false
        }

        if (password.text.toString().trim { it <= ' ' }.isEmpty()) {
            password.requestFocus()
            passwordInputLayout.isErrorEnabled = true
            passwordInputLayout.error = getString(R.string.error_password_required)
            isUiValid = false
            return isUiValid
        } else {
            passwordInputLayout.isErrorEnabled = false
        }

        return isUiValid
    }

    override fun redirectUser() {
        MainActivity.launch(this)
        finish()
    }

    override fun getEmail(): String {
        return email.text.toString()
    }

    override fun setEmail(email: String) {
        this.email.setText(email)
    }

    override fun getPassword(): String {
        return password.text.toString()
    }


    override fun finishActivity() {
        finish()
    }

    override fun hideKeyboard() {
        deliverr.deliverrconsumer.utils.hideKeyboard(this)
    }

    override fun removeEmailError() {
        if (emailInputLayout.isErrorEnabled) {
            emailInputLayout.error = null
            emailInputLayout.isErrorEnabled = false
        }
    }

    override fun removePasswordError() {
        if (passwordInputLayout.isErrorEnabled) {
            passwordInputLayout.error = null
            passwordInputLayout.isErrorEnabled = false
        }
    }

    override fun startHomeActivity() {
        val intent = MainActivity.getLauncherIntent(this)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
    }

    override fun saveAdminDetail(id: String, name: String) {
        deliverrSharedPrefs?.isLogin = true
        deliverrSharedPrefs?.adminName = name
        deliverrSharedPrefs?.adminId = id
    }

    override fun hideProgressBar() {
        progressBar.visibility = View.GONE
    }

    override fun showProgressBar() {
        progressBar.visibility = View.VISIBLE
    }

    companion object {
        fun getLauncherIntent(context: Context): Intent {
            return Intent(context, LoginActivity::class.java)
        }

        @JvmStatic
        fun launch(context: Context) {
            context.startActivity(getLauncherIntent(context))
        }

        fun startControllerNewTask(
            context: Context
        ) {
            val intent = Intent(context, LoginActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(intent)
        }

    }
}

package com.example.customermanageadmin.main.login.rx

import com.google.gson.annotations.SerializedName


class Payload {
    @SerializedName("id")
     val id: String? = null

    @SerializedName("email")
     val email: String? = null

}

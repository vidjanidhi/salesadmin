package com.example.customermanageadmin.main.engineer_list

import androidx.lifecycle.ViewModel

internal interface EngineerDelegate {

    fun isConnectedToInternet(): Boolean
    fun showMessage(stringMessageid: Int)
    fun showMessage(stringMessage: String)
    fun <T : ViewModel> getViewModel(clazz: Class<T>): T
    fun hideProgressBar()
    fun showProgressBar()
    fun showList(transactions: List<Engineer>)

}

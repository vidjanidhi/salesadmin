package com.example.customermanageadmin.main.register.rx

import com.google.gson.annotations.SerializedName

class AddEngineerResponse {
    @SerializedName("success")
    val isSuccess: Boolean = false
    @SerializedName("message")
    val message: String? = null
    @SerializedName("payload")
    val payload: Payload? = null
}

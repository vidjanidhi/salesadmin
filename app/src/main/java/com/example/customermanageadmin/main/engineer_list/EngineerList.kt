package com.example.customermanageadmin.main.engineer_list

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.customermanageadmin.R
import com.example.customermanageadmin.main.StandardActivity
import com.example.customermanageadmin.main.login.rx.RetrofitRxWrapper
import com.example.customermanageadmin.main.register.AddEngineerActivity
import kotlinx.android.synthetic.main.activity_customers_list.progressBar
import kotlinx.android.synthetic.main.activity_engineer_list.*

class EngineerList : StandardActivity(), EngineerDelegate,
    TransactionViewHolder.EngineerClickListener {

    override fun onClickEngineer(engineer: Engineer) {
        AddEngineerActivity.launch(this,engineer)
    }

    private lateinit var transactionsAdapter: EngineerAdapter
    private lateinit var presenter: EngineerPresenter


    override fun hideProgressBar() {
        recyclerView_engineer.visibility = View.VISIBLE
        progressBar.visibility = View.GONE
    }

    override fun showProgressBar() {
        recyclerView_engineer.visibility = View.GONE
        progressBar.visibility = View.VISIBLE
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_engineer_list)

        this.setSupportActionBar(toolbar_eng)

        if (this.supportActionBar != null) {
            this.supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setTitle("Engineers List")
        }


        this.presenter = engineerRealmRepository?.let {
            EngineerPresenter(
                this,
                RetrofitRxWrapper(),
                it
            )
        }!!

    }
    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onResume() {
        super.onResume()
        this.presenter.initialize()
    }
    override fun showList(transactions: List<Engineer>) {
        if (!this::transactionsAdapter.isInitialized) {
            this.transactionsAdapter = EngineerAdapter(transactions,this)
            recyclerView_engineer.adapter = this.transactionsAdapter
            recyclerView_engineer.layoutManager =
                LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        } else {
            this.transactionsAdapter.updateData(transactions)
        }
    }
    companion object {
        fun getLauncherIntent(context: Context): Intent {
            return Intent(context, EngineerList::class.java)
        }

        @JvmStatic
        fun launch(context: Context) {
            context.startActivity(getLauncherIntent(context))
        }
    }
}

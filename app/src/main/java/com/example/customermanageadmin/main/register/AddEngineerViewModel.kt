package com.example.customermanageadmin.main.register

import androidx.lifecycle.ViewModel
import com.example.customermanageadmin.main.engineer_list.Engineer
import com.example.customermanageadmin.main.login.rx.AdminLoginResponse
import com.example.customermanageadmin.main.register.rx.AddEngineerResponse
import io.reactivex.Single

class AddEngineerViewModel : ViewModel() {

    var engineer: Engineer?=null
    var addResponseSingle: Single<AddEngineerResponse>? = null
    var addResponse: AddEngineerResponse ?=null
    var isEditMode: Boolean =false
}

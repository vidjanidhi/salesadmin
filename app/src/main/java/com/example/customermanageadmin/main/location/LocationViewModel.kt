package com.example.customermanageadmin.main.location

import androidx.lifecycle.ViewModel
import com.example.customermanageadmin.main.customers.livedata.CustomerListResponse
import com.example.customermanageadmin.main.engineer_list.EngineerListResponse
import com.example.customermanageadmin.main.login.rx.AdminLoginResponse
import com.example.customermanageadmin.main.register.rx.AddEngineerResponse
import io.reactivex.Single

class LocationViewModel : ViewModel() {


    var current_lat: String=""
    var current_lng: String=""
    var customerListSingle: Single<LocationListResponse>? = null
    var customerListResponse: LocationListResponse ?=null

    var minDistance:Long=0
    var maxDistance:Double= 100.0
    var currentDistanceLimit=20

}

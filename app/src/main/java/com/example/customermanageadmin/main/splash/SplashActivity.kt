package com.example.customermanageadmin.main.splash

import android.os.Bundle
import android.os.Handler
import com.example.customermanageadmin.R
import com.example.customermanageadmin.main.StandardActivity
import com.example.customermanageadmin.main.home.MainActivity
import com.example.customermanageadmin.main.login.LoginActivity
import com.example.customermanageadmin.main.login.rx.RetrofitRxWrapper

class SplashActivity : StandardActivity(),
    SplashDelegate {
    private val SPLASH_DISPLAY_LENGTH = 2500
    private var presenter: SplashActivityPresenter? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        this.presenter = customerRealmRepository?.let {
            engineerRealmRepository?.let { it1 ->
                SplashActivityPresenter(
                    this,
                    RetrofitRxWrapper(),
                    it,
                    it1
                )
            }
        }
    }

    override fun openNext() {
        this.startNextActivity()
    }

    private fun startHomeActivity() {
        MainActivity.launch(this)
        finish()
    }

    private fun startLoginActivity() {
        LoginActivity.launch(this)
        finish()
    }

    override fun onPause() {
        super.onPause()
        cleanUp()
    }

    internal fun cleanUp() {
        if (realm != null && !realm!!.isClosed()) {
            realm!!.close()
        }
    }

    override fun onResume() {
        super.onResume()

        Handler().postDelayed(
            {
                if (isConnectedToInternet()) {
                    presenter?.getCustomerList()
                } else {
                    var count = customerRealmRepository?.countTotalItems()
                    if (count == 0) {
                        showMessage("Connect Internet")
                        finish()
                    } else {
                        openNext()
                    }
                }
            },
            SPLASH_DISPLAY_LENGTH.toLong()
        )  //delay to show activity_splash screen
    }

    fun startNextActivity() {
        if (deliverrSharedPrefs?.isLogin!!) {
            startHomeActivity()
        } else {
            startLoginActivity()
        }

    }

}

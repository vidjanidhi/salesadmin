package com.example.customermanageadmin.main.home.livedata

import androidx.lifecycle.LifecycleOwner
import com.example.customermanageadmin.main.customers.CustomersListActivity
import com.example.customermanageadmin.main.home.MainActivity
import com.example.customermanageadmin.main.home.MainActivityViewDelegate
import dagger.Module
import dagger.Provides
import deliverr.deliverrconsumer.di.scopes.ActivityScope
import deliverr.deliverrconsumer.referral.account_status.CustomerListViewDelegate

@Module
class MainActivityModule {
    @Provides
    @ActivityScope
    fun provideMainActivityViewDelegate(activity: MainActivity): MainActivityViewDelegate =
        activity

    @Provides
    @ActivityScope
    fun provideLifecycleOwner(activity: MainActivity): LifecycleOwner = activity
}

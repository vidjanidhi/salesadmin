package com.example.customermanageadmin.main.location

import android.location.Location
import com.example.customermanageadmin.main.login.rx.RetrofitRxWrapper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers

class LocationPresenter(
    private val delegate: LocationDelegate,
    private val wrapper: RetrofitRxWrapper
) {
    fun initialization() {
        getMaxDistance()
    }

    private var disposable: Disposable? = null

    private val viewModel: LocationViewModel by lazy {
        this.delegate.getViewModel(LocationViewModel::class.java)
    }

    fun changeDistance(distance: Int) {
        viewModel.currentDistanceLimit = distance
        getCustomerList()
    }

    fun getMaxDistance() {
        if (viewModel.customerListSingle == null) {
            viewModel.customerListSingle =
                wrapper.getMaxDistance(viewModel.current_lat, viewModel.current_lng)
        }
        disposable?.dispose()

        disposable = this.viewModel.customerListSingle?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribeWith(object : DisposableSingleObserver<LocationListResponse>() {
                override fun onSuccess(customerListResponse: LocationListResponse) {

                    viewModel.customerListResponse = customerListResponse
                    viewModel.customerListSingle = null
                    if (customerListResponse.isSuccess == true) {
                        var dis = customerListResponse.payload.get(0).distance
                        viewModel.maxDistance = dis.toDouble() + 1
                        delegate.setMin(0)
                        delegate.setMax(viewModel.maxDistance.toLong())
                        getCustomerList()
                    } else {

                    }
                }

                override fun onError(e: Throwable) {
                    viewModel.customerListSingle = null
                    dispose()
                }
            })
    }

    fun getCustomerList() {
        if (viewModel.customerListSingle == null) {
            var limit: Double =
                viewModel.maxDistance - (viewModel.maxDistance * viewModel.currentDistanceLimit / 100)
            viewModel.customerListSingle =

                wrapper.getBrandItemListByLocation(
                    viewModel.current_lat,
                    viewModel.current_lng,
                    limit.toInt()
                )

        }
        disposable?.dispose()

        disposable = this.viewModel.customerListSingle?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribeWith(object : DisposableSingleObserver<LocationListResponse>() {
                override fun onSuccess(customerListResponse: LocationListResponse) {
                    delegate.clearMap()

                    viewModel.customerListResponse = customerListResponse
                    viewModel.customerListSingle = null
                    if (customerListResponse.isSuccess == true) {
                        delegate.setMarkers(viewModel.customerListResponse!!.payload)
                    } else {

                    }
                }

                override fun onError(e: Throwable) {
                    delegate.clearMap()
                    viewModel.customerListSingle = null
                    dispose()
                }
            })
    }

    fun storeLocation(currentLocation: Location?) {
        viewModel.current_lat = currentLocation?.latitude.toString()
        viewModel.current_lng = currentLocation?.longitude.toString()
    }

    fun getcurrentLat(): Double {
        return viewModel.current_lat.toDouble()
    }

    fun getcurrentLng(): Double {
        return viewModel.current_lng.toDouble()
    }

}

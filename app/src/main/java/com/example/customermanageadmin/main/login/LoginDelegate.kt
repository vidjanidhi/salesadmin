package com.example.customermanageadmin.main.login

import androidx.lifecycle.ViewModel

internal interface LoginDelegate {

    fun isConnectedToInternet(): Boolean

    fun getEmail(): String

    fun setEmail(email: String)

    fun getPassword(): String
    fun showMessage(stringMessageid: Int)
    fun showMessage(stringMessage: String)

    fun finishActivity()

    fun hideKeyboard()

    fun removeEmailError()

    fun removePasswordError()

    fun startHomeActivity()

    fun <T : ViewModel> getViewModel(clazz: Class<T>): T
    fun saveAdminDetail(id:String,name:String)
    fun hideProgressBar()
    fun showProgressBar()
    fun redirectUser()

}

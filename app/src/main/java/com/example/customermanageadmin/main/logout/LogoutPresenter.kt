package com.example.customermanageadmin.main.logout

import com.example.customermanageadmin.main.engineer_list.Engineer
import com.example.customermanageadmin.main.login.rx.RetrofitRxWrapper
import com.example.customermanageadmin.main.register.rx.AddEngineerResponse
import com.example.customermanageadmin.realm_model.CustomerRepository
import com.example.customermanageadmin.realm_model.EngineerRepository
import com.example.customermanageadmin.realm_model.RealmManager
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers


class LogoutPresenter(
    private val delegate: LogoutDelegate,
    private val customerRepository: CustomerRepository,
    private val engineerRepository: EngineerRepository,
    private val wrapper: RetrofitRxWrapper
) {

    private var disposable: Disposable? = null

    private val viewModel: LogoutViewModel by lazy {
        this.delegate.getViewModel(LogoutViewModel::class.java)
    }

    fun initialize() {
        if (RealmManager(engineerRepository).checkIfNeedsSync()) {
            syncData()
        } else {
            deleteAllData()
        }
    }

    fun syncData() {
        if (delegate.isConnectedToInternet()) {
            syncWrapper()
        } else {
            delegate.showErrorMessage("Please Connect to internet to sync data")
            delegate.finishCurrent()
        }
    }

    private fun syncWrapper() {
        sendAddedEngineers()
    }

    private fun sendAddedEngineers() {
        var engineers = engineerRepository.findAdds()
        if (engineers.size > 0) {
            addEngineerWrapper(engineers[0])
        } else {
            sendEditedEngineers()
        }

    }

    fun addEngineerWrapper(engineer: Engineer) {

        if (viewModel.addResponseSingle == null) {
            viewModel.addResponseSingle =
                wrapper.addEngineer(
                    engineer.getName(),
                    engineer.getEmail(),
                    engineer.getPhone(),
                    engineer.getAddress(),
                    engineer.getPassword()
                )
        }
        disposable?.dispose()

        disposable = this.viewModel.addResponseSingle?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribeWith(object : DisposableSingleObserver<AddEngineerResponse>() {
                override fun onSuccess(loginResponse: AddEngineerResponse) {

                    viewModel.addResponse = loginResponse
                    viewModel.addResponseSingle = null
                    if (loginResponse.isSuccess == true) {
                        deleteAddedEngineer(engineer.email)
                        sendAddedEngineers()
                    } else {
                        deleteAddedEngineer(engineer.email)
                        sendAddedEngineers()
                    }
                }

                override fun onError(e: Throwable) {
                    viewModel.addResponseSingle = null
                    dispose()
                }
            })
    }

    private fun deleteAddedEngineer(email: String) {
        engineerRepository.deleteAddedItem(email)
    }

    private fun sendEditedEngineers() {
        var engineers = engineerRepository.findEdits()
        if (engineers.size > 0) {
            editEngineerWrapper(engineers[0])
        } else {
            deleteAllData()
        }
    }

    fun editEngineerWrapper(engineer: Engineer) {

        if (viewModel.editResponseSingle == null) {
            viewModel.editResponseSingle =
                wrapper.editEngineer(
                    engineer?.id,
                    engineer.getName(),
                    engineer.getEmail(),
                    engineer.getPhone(),
                    engineer.getAddress()
                )
        }
        disposable?.dispose()

        disposable = this.viewModel.editResponseSingle?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribeWith(object : DisposableSingleObserver<AddEngineerResponse>() {
                override fun onSuccess(editResponse: AddEngineerResponse) {

                    viewModel.editResponse = editResponse
                    viewModel.editResponseSingle = null
                    if (editResponse.isSuccess == true) {
                        deleteEditedEngineer(engineer.id)
                    } else {
                    }
                }

                override fun onError(e: Throwable) {
                    viewModel.editResponseSingle = null
                    dispose()
                }
            })
    }

    private fun deleteEditedEngineer(id: String) {
        engineerRepository.deleteEditedItem(id)
        sendEditedEngineers()
    }

    fun deleteAllData() {
        deleteRealm()
        deletePreferences()
        delegate.showSignInScreen()
    }

    private fun deletePreferences() {
        delegate.clearPreferences()
    }

    fun deleteRealm() {
        customerRepository.deleteAllItems()
        engineerRepository.deleteAllItems()
    }


}

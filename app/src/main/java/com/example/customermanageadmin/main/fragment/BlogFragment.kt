package com.example.customermanageadmin.main.fragment

import android.content.ContextWrapper
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.customermanageadmin.R
import com.example.customermanageadmin.application.MyApplication
import com.example.customermanageadmin.main.StandardActivity
import com.example.customermanageadmin.main.customers.CustomerAnalyticResponse
import com.example.customermanageadmin.main.home.MainActivity
import com.example.customermanageadmin.main.home.MainActivityPresenter
import com.example.customermanageadmin.main.home.MainActivityViewDelegate
import com.example.customermanageadmin.main.home.MainActivityViewModel
import com.example.customermanageadmin.main.home.livedata.FilterData
import com.example.customermanageadmin.main.home.livedata.MainPagedListAdapter
import com.example.customermanageadmin.main.login.rx.RetrofitRxWrapper
import com.example.customermanageadmin.realm_model.CustomerRepository
import com.example.customermanageadmin.realm_model.EngineerRepository
import kotlinx.android.synthetic.main.content_home_screen.*
import kotlinx.android.synthetic.main.nav_header.*
import javax.inject.Inject

class BlogFragment @Inject constructor(
    private val customerRepository: CustomerRepository,
    private val engineerRepository: EngineerRepository,
    private val isNetworkAvailable: Boolean
) :
    Fragment(), MainActivityViewDelegate {

    private var presenter: MainActivityPresenter? = null
    private lateinit var layoutManager: LinearLayoutManager
    private var menuItemsAdapter: MainPagedListAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? =
        inflater.inflate(R.layout.fragment_common, container, false)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        name.text = "fragment1"

        this.presenter = MainActivityPresenter(
            this, RetrofitRxWrapper(),
            customerRepository,
            engineerRepository,
            isNetworkAvailable
        )
        presenter?.getDetail()

    }

    override fun showMessage(stringId: Int) {
    }

    override fun showMessage(message: String) {
    }

    override fun isConnectedToInternet(): Boolean {
        return isNetworkAvailable
    }

    override fun <T : ViewModel> getViewModel(clazz: Class<T>): T {
        var context = this.context
        while (context is ContextWrapper) {
            if (context is MainActivity) {
                return ViewModelProviders.of(context).get(clazz)
            } else if (context is StandardActivity) {
                return ViewModelProviders.of(context).get(clazz)
            }
            context = context.baseContext
        }
        return ViewModelProvider.AndroidViewModelFactory(context?.applicationContext as MyApplication).create(clazz)
    }

    override fun getTodate(): String {
        return ""
    }

    override fun getFromDate(): String {
        return ""
    }

    override fun getName(): String {
        return ""
    }

    override fun hideProgressBar() {
    }

    override fun showProgressBar() {
    }

    override fun setAdapter(it: CustomerAnalyticResponse, filterData: FilterData) {
        setListAdapter(it, filterData)
    }

    private fun setListAdapter(
        customerListResponse: CustomerAnalyticResponse,
        filterData: FilterData
    ) {
        val viewModel: MainActivityViewModel = this.getViewModel(MainActivityViewModel::class.java)


        this.menuItemsAdapter =
            MainPagedListAdapter(filterData, customerListResponse)

        viewModel.initialLoading?.observe(this, androidx.lifecycle.Observer {
            this.presenter?.handleInitialLoadingNetworkState(it)
        })

        viewModel.networkState.observe(this, androidx.lifecycle.Observer {
            this.menuItemsAdapter?.setNetworkState(it)
        })

        viewModel.menuItemsLiveData.observe(this, androidx.lifecycle.Observer {
            this.menuItemsAdapter?.submitList(it)
        })

        this.layoutManager = LinearLayoutManager(
            this.context,
            RecyclerView.VERTICAL,
            false
        )

        recyclerView_customers.layoutManager = this.layoutManager
        recyclerView_customers.adapter = this.menuItemsAdapter
    }

    override fun dismissDialog() {
    }

    override fun showList() {
    }

    override fun notifyAdapter() {
    }
}
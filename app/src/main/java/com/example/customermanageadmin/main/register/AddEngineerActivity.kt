package com.example.customermanageadmin.main.register

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import com.example.customermanageadmin.R
import com.example.customermanageadmin.main.StandardActivity
import com.example.customermanageadmin.main.engineer_list.Engineer
import com.example.customermanageadmin.main.engineer_list.EngineerList
import com.example.customermanageadmin.main.login.rx.RetrofitRxWrapper
import deliverr.deliverrconsumer.utils.isValidEmail
import kotlinx.android.synthetic.main.activity_add_engineer.*

class AddEngineerActivity : StandardActivity(), AddEngineerDelegate {

    private var presenter: AddEngineerPresenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_engineer)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setTitle("Add Engineer")

        bindViews()
        if (getIntent().hasExtra(KEY_ENGINEER)) {
            var engineer = getIntent().extras.getParcelable(KEY_ENGINEER) as Engineer
            setView(engineer)
            supportActionBar?.setTitle("Edit Engineer")
        }
    }

    private fun setView(engineer: Engineer) {
        addButton.setText("Edit")
        name.setText(engineer?.name)
        email.setText(engineer?.email)
        phone.setText(engineer?.phone)
        address.setText(engineer?.address)

        hidePasswordField()

        presenter?.setEditMode(engineer)
    }

    private fun hidePasswordField() {
        passwordInputLayout.visibility = View.GONE
    }

    private fun bindViews() {
        presenter =engineerRealmRepository?.let {
            AddEngineerPresenter(
                this,
                RetrofitRxWrapper(),
                it
            )
        }

            addButton.setOnClickListener({ view1 -> clickAddButton() })

    }

    fun clickAddButton() {
        if (isUiValid()) {
            presenter?.addEngineer()
        }
    }

    fun clickBackButton() {
        finish()
    }

    private fun isUiValid(): Boolean {
        var isUiValid = true

        if (email.text.toString().trim { it <= ' ' }.isEmpty()) {
            emailInputLayout.requestFocus()
            emailInputLayout.isErrorEnabled = true
            emailInputLayout.error = getString(R.string.error_email)
            isUiValid = false
            return isUiValid
        } else {
            emailInputLayout.isErrorEnabled = false
        }

        if (!isValidEmail(email.text.toString())) {
            emailInputLayout.requestFocus()
            emailInputLayout.isErrorEnabled = true
            emailInputLayout.error = getString(R.string.error_valid_email)
            isUiValid = false
            return isUiValid
        } else {
            emailInputLayout.isErrorEnabled = false
        }
        if (presenter?.viewModel?.isEditMode!!) {

        }else{
            if (password.text.toString().trim { it <= ' ' }.isEmpty()) {
                password.requestFocus()
                passwordInputLayout.isErrorEnabled = true
                passwordInputLayout.error = getString(R.string.error_password_required)
                isUiValid = false
                return isUiValid
            } else {
                passwordInputLayout.isErrorEnabled = false
            }
        }

        if (name.text.toString().trim { it <= ' ' }.isEmpty()) {
            name.requestFocus()
            nameInputLayout.isErrorEnabled = true
            nameInputLayout.error = getString(R.string.error_name)
            isUiValid = false
            return isUiValid
        } else {
            nameInputLayout.isErrorEnabled = false
        }

        return isUiValid
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            android.R.id.home -> {
                clickBackButton()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun getEmail(): String {
        return email.text.toString()
    }

    override fun getName(): String {
        return name.text.toString()
    }

    override fun getPhone(): String {
        return phone.text.toString()
    }

    override fun getAddress(): String {
        return address.text.toString()
    }


    override fun getPassword(): String {
        return password.text.toString()
    }

    override fun finishActivity() {
        finish()
    }

    override fun hideKeyboard() {
        deliverr.deliverrconsumer.utils.hideKeyboard(this)
    }

    override fun removeEmailError() {
        if (emailInputLayout.isErrorEnabled) {
            emailInputLayout.error = null
            emailInputLayout.isErrorEnabled = false
        }
    }

    override fun removeNameError() {
        if (nameInputLayout.isErrorEnabled) {
            nameInputLayout.error = null
            nameInputLayout.isErrorEnabled = false
        }
    }

    override fun removePasswordError() {
        if (passwordInputLayout.isErrorEnabled) {
            passwordInputLayout.error = null
            passwordInputLayout.isErrorEnabled = false
        }
    }

    override fun hideProgressBar() {
        progressBar.visibility = View.GONE
    }

    override fun showProgressBar() {
        progressBar.visibility = View.VISIBLE
    }

    override fun clearData() {
        email.setText("")
        password.setText("")
        name.setText("")
        phone.setText("")
        address.setText("")
    }

    companion object {
        private const val KEY_ENGINEER = "key-engineer"
        fun getLauncherIntent(context: Context): Intent {
            return Intent(context, AddEngineerActivity::class.java)
        }

        @JvmStatic
        fun launch(context: Context) {
            context.startActivity(getLauncherIntent(context))
        }

        fun launch(context: EngineerList, engineer: Engineer) {
            context.startActivity(getLauncherIntent(context, engineer))
        }

        fun getLauncherIntent(
            context: Context,
            engineer: Engineer
        ): Intent {
            return Intent(context, AddEngineerActivity::class.java).apply {
                putExtra(KEY_ENGINEER,engineer)
            }
        }
    }
}

package com.example.customermanageadmin.main.engineer_list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.example.customermanageadmin.R
import com.example.customermanageadmin.main.register.AddEngineerActivity

class EngineerAdapter(
    private var engineers: List<Engineer>,
    private val clickListener: TransactionViewHolder.EngineerClickListener
) : RecyclerView.Adapter<TransactionViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TransactionViewHolder {
        return TransactionViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.list_item_engineer,
                parent,
                false
            ),clickListener
        )
    }

    override fun getItemCount(): Int = this.engineers.size

    override fun onBindViewHolder(holder: TransactionViewHolder, position: Int) {
        holder.bind(engineers[position])
    }

    fun updateData(engineers: List<Engineer>) {
        this.engineers = engineers
        this.notifyDataSetChanged()
    }
}

class TransactionViewHolder(itemView: View,
                            private val clickListener: EngineerClickListener
) : RecyclerView.ViewHolder(itemView) {
    fun bind(engineer: Engineer) {
        itemView.findViewById<AppCompatTextView>(R.id.title).text = engineer.name
        itemView.findViewById<AppCompatTextView>(R.id.email).text = engineer.email
        itemView.findViewById<AppCompatTextView>(R.id.phone).text = engineer.phone
        itemView.findViewById<AppCompatTextView>(R.id.address).text = engineer.address
        itemView.findViewById<AppCompatImageView>(R.id.edit_button).setOnClickListener {
          clickListener.onClickEngineer(engineer)
        }

    }
    interface EngineerClickListener {
        fun onClickEngineer(engineer: Engineer)
    }
}

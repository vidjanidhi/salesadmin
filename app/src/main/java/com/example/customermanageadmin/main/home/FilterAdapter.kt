package com.example.customermanageadmin.main.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.customermanageadmin.R
import com.example.customermanageadmin.models.FilterType

class FilterAdapter(
    private var list: ArrayList<String>,
    private val clickListener: TransactionViewHolder.CategoryClickListener,
    private var selectedPosition: Int?
) : RecyclerView.Adapter<TransactionViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TransactionViewHolder {
        return TransactionViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.list_item_category,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int = this.list.size

    override fun onBindViewHolder(holder: TransactionViewHolder, position: Int) {

        if (selectedPosition == position) {
            holder.bind(list.get(position), true)
        } else {
            holder.bind(list.get(position), false)
        }

        holder.itemView.setOnClickListener {
            selectedPosition=position
            clickListener.onClickCategory(list.get(position),position)
        }
    }
}

class TransactionViewHolder(
    itemView: View
) : RecyclerView.ViewHolder(itemView) {

    fun bind(engineer: String, isSelected: Boolean) {

        var textview = itemView.findViewById<AppCompatTextView>(R.id.name)
        textview.text = engineer
        if (isSelected) {
            textview.setTextColor(ContextCompat.getColor(itemView.context, R.color.black))
        } else {
            textview.setTextColor(ContextCompat.getColor(itemView.context, R.color.colorAccent))
        }


    }

    interface CategoryClickListener {
        fun onClickCategory(engineer: String,position:Int)
    }
}

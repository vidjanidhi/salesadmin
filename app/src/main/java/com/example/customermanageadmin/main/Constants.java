package com.example.customermanageadmin.main;

import com.example.customermanageadmin.prefs.SharedPrefsService;

import javax.inject.Inject;


public class Constants {
    @Inject
    SharedPrefsService deliverrSharedPrefs;

    @Inject
    public Constants() {
    }

    public static final String base_url = "https://appnutzz.com/server/customer_sales/";

    public Constants(SharedPrefsService deliverrSharedPrefs) {
        this.deliverrSharedPrefs = deliverrSharedPrefs;
    }

    public static class Admin {
        public static final String LOGIN = "api.php?req=admin_login";
        public static final String ADD_ENGINEER = "api.php?req=engineer_insert";
        public static final String UPDATE_ENGINEER = "api.php?req=engineer_update";
        public static final String GET_CUSTOMERS = "api.php?req=get_customers";
        public static final String GET_CUSTOMERS_BY_DISTANCE = "api.php?req=get_engineers_by_distance";
        public static final String GET_MAX_DISTANCE = "api.php?req=get_max_distance";
        public static final String GET_ENGINEERS = "api.php?req=get_engineers";
        public static final String GET_CUSTOMERS_DETAIL = "api.php?req=get_customers_detail";

    }


}

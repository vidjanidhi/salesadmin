package com.example.customermanageadmin.main.home.livedata

import java.util.*

class FilterData {
    var name: String = ""
    var from_date: String = ""
    var to_Date: String = ""

    var fromDate: Date = Date()
    var toDate: Date = Date()

    var paymentMode: Int = 0
    var engineerId: String = ""
}

package com.example.customermanageadmin.main.register

import androidx.lifecycle.ViewModel

internal interface AddEngineerDelegate {

    fun isConnectedToInternet(): Boolean

    fun getEmail(): String
    fun getName(): String
    fun getPhone(): String
    fun getAddress(): String


    fun getPassword(): String
    fun showMessage(stringMessageid: Int)
    fun showMessage(stringMessage: String)

    fun finishActivity()

    fun hideKeyboard()

    fun removeEmailError()

    fun removePasswordError()


    fun <T : ViewModel> getViewModel(clazz: Class<T>): T
    fun hideProgressBar()
    fun showProgressBar()
    fun removeNameError()
    fun clearData()

}

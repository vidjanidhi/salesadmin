package deliverr.deliverrconsumer.screens.add_item.store_categories_grid_view.brand_items_screen

import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import com.example.customermanageadmin.application.utils.LOADED
import com.example.customermanageadmin.application.utils.LOADING
import com.example.customermanageadmin.application.utils.NetworkState
import com.example.customermanageadmin.application.utils.NetworkStatus
import com.example.customermanageadmin.main.customers.livedata.Customer
import com.example.customermanageadmin.main.customers.livedata.CustomerListResponse
import com.example.customermanageadmin.main.login.rx.RetrofitRxWrapper
import com.example.customermanageadmin.realm_model.CustomerRealmRepository
import com.example.customermanageadmin.realm_model.CustomerRepository
import io.realm.Realm
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

const val INITIAL_PAGE_SIZE = 10
const val FUTURE_PAGE_SIZE = 5

class CustomerListDataSource(
    private val wrapper: RetrofitRxWrapper,
    private val customerRepository: CustomerRepository,
    private val isNetworkAvailable: Boolean
) : PageKeyedDataSource<Int, Customer>() {
    val initialLoading: MutableLiveData<NetworkState> = MutableLiveData()
    val networkState: MutableLiveData<NetworkState> = MutableLiveData()

    private var retryInitialLoad = true
    private lateinit var loadInitialParams: LoadInitialParams<Int>
    private lateinit var loadInitialCallback: LoadInitialCallback<Int, Customer>
    private lateinit var loadParams: LoadParams<Int>
    private lateinit var loadCallback: LoadCallback<Int, Customer>


    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, Customer>
    ) {
        this.initialLoading.postValue(LOADING)
        this.networkState.postValue(LOADING)

        this.retryInitialLoad = true
        this.loadInitialParams = params
        this.loadInitialCallback = callback


        if (!isNetworkAvailable) {
            val realm = Realm.getDefaultInstance()
            val customerRealmRepository = CustomerRealmRepository(realm)

            var customers: List<Customer> = customerRealmRepository?.findAll()
            val prodObjCopy = realm.copyFromRealm(customers)

            callback.onResult(
                prodObjCopy,
                null,
                INITIAL_PAGE_SIZE
            )
            initialLoading.postValue(LOADED)
            networkState.postValue(LOADED)

            customerRealmRepository.closeRealm()

        } else {
            this.wrapper.getBrandItemListCall(
                0,
                (INITIAL_PAGE_SIZE)
            ).enqueue(object : Callback<CustomerListResponse> {
                override fun onFailure(call: Call<CustomerListResponse>, t: Throwable?) {
                    val errorMessage = t?.message ?: "Unknown error"
                    initialLoading.postValue(NetworkState(NetworkStatus.FAILED, errorMessage))
                    networkState.postValue(NetworkState(NetworkStatus.FAILED, errorMessage))
                }

                override fun onResponse(
                    call: Call<CustomerListResponse>,
                    response: Response<CustomerListResponse>?
                ) {
                    if ((response != null) && response.isSuccessful) {
                        if (response.body()!!.isSuccess) {
                            /*val realm = Realm.getDefaultInstance()
                            val customerRealmRepository = CustomerRealmRepository(realm)
                            customerRealmRepository.deleteAllItems()
                            customerRealmRepository?.insertOrUpdate(response.body()?.payload)
                            customerRealmRepository.closeRealm()*/

                            callback.onResult(
                                response.body()!!.payload,
                                null,
                                INITIAL_PAGE_SIZE
                            )
                            initialLoading.postValue(LOADED)
                            networkState.postValue(LOADED)
                        }
                    } else {
                        initialLoading.postValue(
                            NetworkState(
                                NetworkStatus.FAILED,
                                response!!.message()
                            )
                        )
                        networkState.postValue(
                            NetworkState(
                                NetworkStatus.FAILED,
                                response.message()
                            )
                        )
                    }
                }
            })
        }
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Customer>) {
        networkState.postValue(LOADING)

        this.retryInitialLoad = false
        this.loadParams = params
        this.loadCallback = callback

        if (isNetworkAvailable) {
            this.wrapper.getBrandItemListCall(
                params.key,
                params.requestedLoadSize
            ).enqueue(object : Callback<CustomerListResponse> {
                override fun onFailure(call: Call<CustomerListResponse>, t: Throwable?) {
                    val errorMessage = t?.message ?: "Unknown error"
                    networkState.postValue(NetworkState(NetworkStatus.FAILED, errorMessage))
                }

                override fun onResponse(
                    call: Call<CustomerListResponse>,
                    response: Response<CustomerListResponse>?
                ) {
                    if ((response != null) && response.isSuccessful) {
                        if (response.body()!!.isSuccess) {
                            /* val realm = Realm.getDefaultInstance()
                             val customerRealmRepository = CustomerRealmRepository(realm)
                             customerRealmRepository.insertOrUpdate(response.body()?.payload)
                             customerRealmRepository.closeRealm()*/

                            var total: String = response.body()!!.total
                            val itemsToSkipNext =
                                if (total.toLong() <= (params.key + params.requestedLoadSize)) null else (params.key + params.requestedLoadSize)
                            callback.onResult(
                                response.body()!!.payload,
                                itemsToSkipNext
                            )
                            networkState.postValue(LOADED)
                        }
                    } else {
                        networkState.postValue(
                            NetworkState(
                                NetworkStatus.FAILED,
                                response!!.message()
                            )
                        )
                    }
                }
            })
        }
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Customer>) {
        // This is not implemented because we only load next items
    }

    fun retryLoading() {
        if (this.retryInitialLoad) {
            this.loadInitial(this.loadInitialParams, this.loadInitialCallback)
        } else {
            this.loadAfter(this.loadParams, this.loadCallback)
        }
    }
}

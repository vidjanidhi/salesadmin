package com.example.customermanageadmin.main.register

import android.text.Editable
import android.text.TextWatcher
import com.example.customermanageadmin.base.BaseServiceRxWrapper
import com.example.customermanageadmin.main.engineer_list.Engineer
import com.example.customermanageadmin.main.login.rx.RetrofitRxWrapper
import com.example.customermanageadmin.main.register.rx.AddEngineerResponse
import com.example.customermanageadmin.realm_model.EngineerRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import java.net.UnknownHostException

internal class AddEngineerPresenter(
    private val delegate: AddEngineerDelegate,
    private val wrapper: RetrofitRxWrapper,
    private val engineerRepository: EngineerRepository
) : TextWatcher {

    val viewModel: AddEngineerViewModel by lazy {
        this.delegate.getViewModel(AddEngineerViewModel::class.java)
    }
    private var disposable: Disposable? = null
    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

    }

    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
        val text = s.toString()
        if (text.length > 0) {
            if (text == delegate.getEmail()) {
                delegate.removeEmailError()
            }
            if (text == delegate.getPassword()) {
                delegate.removePasswordError()
            }
            if (text == delegate.getName()) {
                delegate.removeNameError()
            }
        }
    }

    override fun afterTextChanged(s: Editable) {

    }

    fun addEngineer() {
        if (viewModel.isEditMode) {
            if (delegate.isConnectedToInternet()) {
                delegate.hideKeyboard()
                delegate.showProgressBar()
                editEngineerWrapper()
            } else {
                editEngineerWrapperRealm()
//                delegate.showMessage(R.string.no_internet_connection_message)
            }
        } else {
            if (delegate.isConnectedToInternet()) {
                delegate.hideKeyboard()
                delegate.showProgressBar()
                addEngineerWrapper()
            } else {
                addEngineerWrapperRealm()
//                delegate.showMessage(R.string.no_internet_connection_message)
            }
        }

    }

    fun editEngineerWrapperRealm() {
        var engineer = Engineer()

        engineer.id = viewModel.engineer?.id!!
        engineer.name = delegate.getName()
        engineer.email = delegate.getEmail()
        engineer.phone = delegate.getPhone()
        engineer.address = delegate.getAddress()

//        engineerRepository.insertOrUpdate(engineer)

        engineer.edit = true

        var count1 = engineerRepository.countTotalItems()
        engineerRepository.insertEditedItem(engineer,engineer.id)
        var count = engineerRepository.countTotalItems()

        delegate.showMessage("You are offline..store to local")
    }

    fun addEngineerWrapperRealm() {
        var engineer = Engineer()

        engineer.id = "0"
        engineer.name = delegate.getName()
        engineer.email = delegate.getEmail()
        engineer.phone = delegate.getPhone()
        engineer.address = delegate.getAddress()
        engineer.password = delegate.getPassword()

//        engineerRepository.insertOrUpdate(engineer)

        engineer.add = true

        var count1 = engineerRepository.countTotalItems()
        engineerRepository.insertAddedItem(engineer,engineer.id)

        var count = engineerRepository.countTotalItems()

        delegate.showMessage("You are offline..store to local")
        delegate.clearData()
    }

    fun addEngineerWrapper() {

        if (viewModel.addResponseSingle == null) {
            viewModel.addResponseSingle =
                wrapper.addEngineer(
                    delegate.getName(),
                    delegate.getEmail(),
                    delegate.getPhone(),
                    delegate.getAddress(),
                    delegate.getPassword()
                )
        }
        disposable?.dispose()

        disposable = this.viewModel.addResponseSingle?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribeWith(object : DisposableSingleObserver<AddEngineerResponse>() {
                override fun onSuccess(loginResponse: AddEngineerResponse) {

                    viewModel.addResponse = loginResponse
                    viewModel.addResponseSingle = null
                    if (loginResponse.isSuccess == true) {
                        loginSuccess()
                    } else {
                        loginResponse.message?.let { loginFailed(it) }
                    }
                }

                override fun onError(e: Throwable) {
                    viewModel.addResponseSingle = null
                    var message: String
                    if (e is UnknownHostException) {
                        message = BaseServiceRxWrapper.NO_INTERNET_CONNECTION
                    } else {
                        message = e.message.toString()
                    }
                    loginFailed(message)
                    dispose()
                }
            })
    }

    private fun loginSuccess() {
        if (viewModel.isEditMode) {
            delegate.showMessage("Edit Successfully")
        } else {
            delegate.showMessage("Added Successfully")
            delegate.clearData()
        }

        delegate.hideProgressBar()

    }

    private fun loginFailed(message: String) {
        delegate.hideProgressBar()
        delegate.showMessage(message)
    }

    fun setEditMode(engineer: Engineer) {
        viewModel.isEditMode = true
        viewModel.engineer = engineer
    }

    fun editEngineerWrapper() {

        if (viewModel.addResponseSingle == null) {
            viewModel.addResponseSingle =
                wrapper.editEngineer(
                    viewModel.engineer?.id!!,
                    delegate.getName(),
                    delegate.getEmail(),
                    delegate.getPhone(),
                    delegate.getAddress()
                )
        }
        disposable?.dispose()

        disposable = this.viewModel.addResponseSingle?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribeWith(object : DisposableSingleObserver<AddEngineerResponse>() {
                override fun onSuccess(loginResponse: AddEngineerResponse) {

                    viewModel.addResponse = loginResponse
                    viewModel.addResponseSingle = null
                    if (loginResponse.isSuccess == true) {
                        loginSuccess()
                    } else {
                        loginResponse.message?.let { loginFailed(it) }
                    }
                }

                override fun onError(e: Throwable) {
                    viewModel.addResponseSingle = null
                    var message: String
                    if (e is UnknownHostException) {
                        message = BaseServiceRxWrapper.NO_INTERNET_CONNECTION
                    } else {
                        message = e.message.toString()
                    }
                    loginFailed(message)
                    dispose()
                }
            })
    }
}

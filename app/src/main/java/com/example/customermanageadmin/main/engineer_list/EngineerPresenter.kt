package com.example.customermanageadmin.main.engineer_list

import com.example.customermanageadmin.base.BaseServiceRxWrapper
import com.example.customermanageadmin.main.login.rx.RetrofitRxWrapper
import com.example.customermanageadmin.realm_model.EngineerRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import java.net.UnknownHostException

internal class EngineerPresenter(
    private val engineerDelegate: EngineerDelegate,
    private val wrapper: RetrofitRxWrapper,
    private val engineerRepository: EngineerRepository
) {

    val viewModel: EngineerViewModel by lazy {
        this.engineerDelegate.getViewModel(EngineerViewModel::class.java)
    }
    private var disposable: Disposable? = null

    fun initialize() {
        if (engineerDelegate.isConnectedToInternet()) {
            engineerDelegate.showProgressBar()
            getEngineers()
        } else {
            viewModel.engineerListResponse=EngineerListResponse()
            viewModel.engineerListResponse?.payload = engineerRepository.findAll()
            engineerDelegate.showMessage("Local Data")
            success()
        }
    }


    fun getEngineers() {

        if (viewModel.engineerResponseSingle == null) {
            viewModel.engineerResponseSingle =
                wrapper.getEngineers()
        }
        disposable?.dispose()

        disposable = this.viewModel.engineerResponseSingle?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribeWith(object : DisposableSingleObserver<EngineerListResponse>() {
                override fun onSuccess(loginResponse: EngineerListResponse) {

                    viewModel.engineerListResponse = loginResponse
                    viewModel.engineerResponseSingle = null
                    if (loginResponse.isSuccess == true) {
                        success()
                    } else {
                        loginResponse.message?.let { failed(it) }
                    }
                }

                override fun onError(e: Throwable) {
                    viewModel.engineerResponseSingle = null
                    var message: String
                    if (e is UnknownHostException) {
                        message = BaseServiceRxWrapper.NO_INTERNET_CONNECTION
                    } else {
                        message = e.message.toString()
                    }
                    failed(message)
                    dispose()
                }
            })
    }

    private fun success() {
        engineerDelegate.hideProgressBar()
        viewModel.engineerListResponse?.payload?.let { engineerDelegate.showList(it) }

    }

    private fun failed(message: String) {
        engineerDelegate.hideProgressBar()
        engineerDelegate.showMessage(message)
    }

}

package com.example.customermanageadmin.main.location

import androidx.lifecycle.ViewModel

interface LocationDelegate {



    fun isConnectedToInternet(): Boolean
    fun getSeekPosition():Int
    fun setMaxDistance(dis:Long)
    fun <T : ViewModel> getViewModel(clazz: Class<T>): T

    fun setMin(distance: Long)
    fun setMax(distance: Long)
    fun setMarkers(list: List<CustomerLocation>)
    fun clearMap()
}

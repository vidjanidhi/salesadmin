package com.example.customermanageadmin.main.login

import androidx.lifecycle.ViewModel
import com.example.customermanageadmin.main.login.rx.AdminLoginResponse
import io.reactivex.Single

class LoginViewModel : ViewModel() {

    var loginSingle: Single<AdminLoginResponse>? = null
    var loginResponse: AdminLoginResponse ?=null
}

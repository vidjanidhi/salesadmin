package com.example.customermanageadmin.main.engineer_list

import androidx.lifecycle.ViewModel
import com.example.customermanageadmin.main.login.rx.AdminLoginResponse
import io.reactivex.Single

class EngineerViewModel : ViewModel() {

    var engineerResponseSingle: Single<EngineerListResponse>? = null
    var engineerListResponse: EngineerListResponse ?=null
}

package com.example.customermanageadmin.main.location;

import com.example.customermanageadmin.main.engineer_list.Engineer;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LocationListResponse {
    @SerializedName("success")
    boolean success;
    @SerializedName("message")
    String message;

    @SerializedName("payload")
    private List<CustomerLocation> payload;

    public boolean isSuccess() {
        return success;
    }

    public String getMessage() {
        return message;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setPayload(List<CustomerLocation> payload) {
        this.payload = payload;
    }

    public List<CustomerLocation> getPayload() {
        return payload;
    }
}

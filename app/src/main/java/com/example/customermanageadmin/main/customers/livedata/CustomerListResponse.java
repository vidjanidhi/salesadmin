package com.example.customermanageadmin.main.customers.livedata;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CustomerListResponse {
    @SerializedName("success")
    boolean success;
    @SerializedName("message")
    String message;
    @SerializedName("total")
    String total;

    @SerializedName("payload")
    private List<Customer> payload;

    public boolean isSuccess() {
        return success;
    }

    public String getMessage() {
        return message;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public void setPayload(List<Customer> payload) {
        this.payload = payload;
    }

    public List<Customer> getPayload() {
        return payload;
    }
}

package com.example.customermanageadmin.main.logout

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.example.customermanageadmin.R
import com.example.customermanageadmin.main.StandardActivity
import com.example.customermanageadmin.main.login.LoginActivity
import com.example.customermanageadmin.main.login.rx.RetrofitRxWrapper

class LogoutActivity : StandardActivity(), LogoutDelegate {
    private var presenter: LogoutPresenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_logout)

        if (presenter == null) {
            this.presenter = customerRealmRepository?.let {
                engineerRealmRepository?.let { it1 ->
                    LogoutPresenter(
                        this,
                        it,
                        it1,
                        RetrofitRxWrapper()

                    )
                }
            }
        }
        presenter?.initialize()


    }

    override fun finishCurrent() {
        finish()
    }

    override fun showSignInScreen() {
        LoginActivity.startControllerNewTask(
            this
        )
    }

    override fun showErrorMessage(message: String) {
        showMessage(message)
    }

    override fun clearPreferences() {
        deliverrSharedPrefs?.clearPreferences()
    }


    companion object {
        fun launch(context: Context) {
            val launcherIntent = Intent(context, LogoutActivity::class.java)
            context.startActivity(launcherIntent)
        }
    }

}

package deliverr.deliverrconsumer.referral.account_status

import androidx.annotation.StringRes
import androidx.lifecycle.ViewModel
import com.example.customermanageadmin.main.customers.CustomerAnalyticResponse
import com.example.customermanageadmin.main.customers.livedata.CustomerListResponse

interface CustomerListViewDelegate {

    fun showProgressBar()
    fun hideProgressBar()

    fun showMessage(message: String)
    fun showMessage(@StringRes messageId: Int)

    fun isConnectedToInternet(): Boolean
    fun <T : ViewModel> getViewModel(clazz: Class<T>): T

    fun setAdapter(customerListResponse: CustomerAnalyticResponse)
}

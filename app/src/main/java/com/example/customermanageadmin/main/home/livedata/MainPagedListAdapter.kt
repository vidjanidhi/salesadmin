package com.example.customermanageadmin.main.home.livedata

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.customermanageadmin.R
import com.example.customermanageadmin.application.utils.NetworkState
import com.example.customermanageadmin.main.customers.CustomerAnalyticResponse
import com.example.customermanageadmin.main.customers.livedata.Customer
import com.example.customermanageadmin.main.customers.livedata.CustomerHeaderViewHolder
import com.example.customermanageadmin.main.customers.livedata.CustomerViewHolder
import com.example.customermanageadmin.main.customers.livedata.SpaceViewHolder

private const val LAYOUT_TYPE_ITEM = 0
private const val LAYOUT_TYPE_ITEM_HEADER = 1

class MainPagedListAdapter(
    val filterData: FilterData,
    val customerListResponse: CustomerAnalyticResponse
) : PagedListAdapter<Customer, RecyclerView.ViewHolder>(
    Customer.DIFF_CALLBACK
) {

    private var networkState: NetworkState? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)

        return when (viewType) {

            LAYOUT_TYPE_ITEM_HEADER -> {
                val itemView = layoutInflater.inflate(
                    R.layout.list_item_sales_feeds_header,
                    parent,
                    false
                )
                MainHeaderViewHolder(itemView)
            }
            LAYOUT_TYPE_ITEM -> {
                val itemView = layoutInflater.inflate(
                    R.layout.list_item_sales_feeds,
                    parent,
                    false
                )
                CustomerViewHolder(itemView)
            }

            else -> {
                val itemView = layoutInflater.inflate(
                    R.layout.list_item_sales_feeds,
                    parent,
                    false
                )
                SpaceViewHolder(itemView)
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is CustomerViewHolder -> {
                val item = this.getItem(position - 1)
                if (item != null) {
                    item?.let { holder.bind(it) }
                }
            }
            is MainHeaderViewHolder -> {
                if (customerListResponse != null) {
                    customerListResponse?.let { holder.bind(it,filterData) }
                }
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (position) {
            0 -> LAYOUT_TYPE_ITEM_HEADER
            else -> LAYOUT_TYPE_ITEM
        }

    }

    override fun getItemCount(): Int {
        return super.getItemCount() + 1
    }

    fun setNetworkState(newState: NetworkState) {
        this.networkState = newState
        this.notifyDataSetChanged()
    }


}

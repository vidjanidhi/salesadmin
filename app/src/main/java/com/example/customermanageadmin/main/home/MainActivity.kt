package com.example.customermanageadmin.main.home

import android.app.Activity
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.*
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatSpinner
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.archit.calendardaterangepicker.customviews.DateRangeCalendarView
import com.example.customermanageadmin.R
import com.example.customermanageadmin.application.utils.PermissionRequestCodes
import com.example.customermanageadmin.application.utils.PermissionUtils
import com.example.customermanageadmin.main.StandardActivity
import com.example.customermanageadmin.main.customers.CustomerAnalyticResponse
import com.example.customermanageadmin.main.customers.CustomersListActivity
import com.example.customermanageadmin.main.engineer_list.EngineerList
import com.example.customermanageadmin.main.fragment.BlogFragment
import com.example.customermanageadmin.main.home.livedata.FilterData
import com.example.customermanageadmin.main.home.livedata.MainPagedListAdapter
import com.example.customermanageadmin.main.location.LocationActivity
import com.example.customermanageadmin.main.login.rx.RetrofitRxWrapper
import com.example.customermanageadmin.main.logout.LogoutActivity
import com.example.customermanageadmin.main.register.AddEngineerActivity
import com.example.customermanageadmin.models.FilterType
import com.example.customermanageadmin.models.PaymentMode
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_home_screen.*
import kotlinx.android.synthetic.main.nav_header.*
import kotlinx.android.synthetic.main.nav_header.view.*
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class MainActivity : StandardActivity(), MainActivityViewDelegate,
    TransactionViewHolder.CategoryClickListener {

    var companies_list = PaymentMode.getPaymentModeList(true)
    private lateinit var filterAdapter: FilterAdapter

    private var menu: Menu? = null
    private var presenter: MainActivityPresenter? = null

    private lateinit var layoutManager: LinearLayoutManager
    private var menuItemsAdapter: MainPagedListAdapter? = null

    lateinit var fromdate: EditText
    lateinit var todate: EditText
    lateinit var name: EditText
    lateinit var paymentMode: AppCompatSpinner
    private var dateFormatter: SimpleDateFormat = SimpleDateFormat("dd-MM-yyyy", Locale.US)

    var dialog: Dialog? = null
    var fromDatePickerDialog: DatePickerDialog? = null
    var toDatePickerDialog: DatePickerDialog? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        this.bindViews()
        presenter?.getDetail()
        var navigation: BottomNavigationView = findViewById(R.id.bottom_navigationView)
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        val fragment = deliverrSharedPrefs?.let {
            customerRealmRepository?.let { it1 ->
                engineerRealmRepository?.let { it2 ->
                    BlogFragment(
                        it1, it2, isConnectedToInternet()
                    )
                }
            }
        }
        if (fragment != null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, fragment, fragment.javaClass.getSimpleName())
                .commit()
        }
    }

    private val mOnNavigationItemSelectedListener =
        BottomNavigationView.OnNavigationItemSelectedListener { menuItem ->
            when (menuItem.itemId) {
                R.id.f1 -> {
                    val fragment = deliverrSharedPrefs?.let {
                        customerRealmRepository?.let { it1 ->
                            engineerRealmRepository?.let { it2 ->
                                BlogFragment(
                                    it1, it2, isConnectedToInternet()
                                )
                            }
                        }

                    }
                    if (fragment != null) {
                        supportFragmentManager.beginTransaction()
                            .replace(R.id.container, fragment, fragment.javaClass.getSimpleName())
                            .commit()
                    }
                    return@OnNavigationItemSelectedListener true
                }
                R.id.f2 -> {
                    val fragment = ChapterFragment()
                    supportFragmentManager.beginTransaction()
                        .replace(R.id.container, fragment, fragment.javaClass.getSimpleName())
                        .commit()
                    return@OnNavigationItemSelectedListener true
                }
                R.id.f3 -> {
                    val fragment = StoreFragment()
                    supportFragmentManager.beginTransaction()
                        .replace(R.id.container, fragment, fragment.javaClass.getSimpleName())
                        .commit()
                    return@OnNavigationItemSelectedListener true
                }
            }
            false
        }


    class ChapterFragment : Fragment() {
        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? =
            inflater.inflate(R.layout.fragment_common_one, container, false)

        override fun onActivityCreated(savedInstanceState: Bundle?) {
            super.onActivityCreated(savedInstanceState)
            name.text = "fragment2"
        }
    }


    class StoreFragment : Fragment() {
        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? =
            inflater.inflate(R.layout.fragment_common_two, container, false)

        override fun onActivityCreated(savedInstanceState: Bundle?) {
            super.onActivityCreated(savedInstanceState)
            name.text = "fragment3"
        }
    }

    private fun bindViews() {
        if (presenter == null) {
            this.presenter = customerRealmRepository?.let {
                engineerRealmRepository?.let { it1 ->
                    MainActivityPresenter(
                        this,
                        RetrofitRxWrapper(),
                        it, it1, isConnectedToInternet()
                    )
                }
            }
        }

        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)

        this.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbar.setNavigationIcon(R.drawable.ic_drawer_orange)
        supportActionBar?.setTitle("DashBoard")

        navigationView.setNavigationItemSelectedListener(this::onClickNavigationDrawerItem)
        navigationView.getHeaderView(0).name.setText(deliverrSharedPrefs?.adminName)
        showList()
    }

    override fun showList() {
        if (!this::filterAdapter.isInitialized) {
            this.filterAdapter =
                FilterAdapter(FilterType.getCategoryList(), this, presenter?.getSelectedPosition())
            recyclerView_category.adapter = this.filterAdapter
            recyclerView_category.layoutManager =
                LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)

            onClickCategory(FilterType.TODAY, position = 0)
        }
    }

    override fun notifyAdapter() {
        filterAdapter.notifyDataSetChanged()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        this.menuInflater.inflate(R.menu.main_menu, menu)
        this.menu = menu
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            android.R.id.home -> {
                drawer_layout.openDrawer(GravityCompat.START)
                true
            }
            R.id.date_range -> {
                ShowFilter()
                true
            }
            R.id.sync -> {
                showProgressBar()
                syncData()
                true
            }
            R.id.location -> {
                if (isConnectedToInternet()) {
                    if (this.handleLocationPermissions()) {
                        LocationActivity.launch(this)
                    } else {
                        showMessage("Please accept location permission")
                    }
                } else {
                    showMessage("Please connect to internet")
                }

                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun syncData() {
        if (isConnectedToInternet()) {
            presenter?.sync()
        } else {
            showMessage("Connect Internet")
            hideProgressBar()
        }
    }

    fun onClickNavigationDrawerItem(menuItem: MenuItem): Boolean {
        when (menuItem.itemId) {
            R.id.add_engineer -> {
                AddEngineerActivity.launch(this)
            }
            R.id.customer_feeds -> {
                CustomersListActivity.launch(this)
            }

            R.id.engineer_list -> {
                EngineerList.launch(this)
            }
            R.id.logout -> {
                LogoutActivity.launch(this)
            }
        }
        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    private fun showDialog() {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.dialog_date_range)

        var okButton = dialog.findViewById<AppCompatButton>(R.id.ok)
        var cancelButton =
            dialog.findViewById<AppCompatButton>(R.id.cancel)
        var calendar_view =
            dialog.findViewById<DateRangeCalendarView>(R.id.calendar_view)

        var nextYear: Calendar = Calendar.getInstance()
        nextYear.add(Calendar.MONTH, 10)
        nextYear.add(Calendar.YEAR, 2019)

        var lastYear: Calendar = Calendar.getInstance()
        lastYear.add(Calendar.MONTH, 10)
        lastYear.add(Calendar.YEAR, 2018)

        val today = Date()

        okButton.setOnClickListener {
            dialog.dismiss()
        }
        cancelButton.setOnClickListener { dialog.dismiss() }

        var startMonth: Calendar = Calendar.getInstance();
        startMonth.add(Calendar.MONTH, -2);
        var endMonth: Calendar = Calendar.getInstance();
        endMonth.add(Calendar.MONTH, 5)
        calendar_view.setSelectedDateRange(startMonth, startMonth);
        calendar_view.setVisibleMonthRange(startMonth, endMonth);
//        calendar_view.setVisibleMonthRange(lastYear, nextYear);

        calendar_view.setCalendarListener(object : DateRangeCalendarView.CalendarListener {
            override fun onFirstDateSelected(startDate: Calendar) {
                Toast.makeText(
                    this@MainActivity,
                    "Start Date: " + startDate.time.toString(),
                    Toast.LENGTH_SHORT
                ).show()
            }

            override fun onDateRangeSelected(startDate: Calendar, endDate: Calendar) {
                Toast.makeText(
                    this@MainActivity,
                    "Start Date: " + startDate.time.toString() + " End date: " + endDate.time.toString(),
                    Toast.LENGTH_SHORT
                ).show()
            }
        })
        dialog.show()

    }

    companion object {
        const val REQUEST_LOCATION_SERVICE = 8081
        fun getLauncherIntent(context: Context): Intent {
            return Intent(context, MainActivity::class.java)
        }

        @JvmStatic
        fun launch(context: Context) {
            context.startActivity(getLauncherIntent(context))
        }
    }


    private fun ShowFilter() {

        val sdf = SimpleDateFormat("dd-MM-yyyy")
        val inflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val menuLayout = inflater.inflate(R.layout.dialog_date_range_filter, null)
        val dg = AlertDialog.Builder(this)
        fromdate = menuLayout.findViewById(R.id.fromdate) as EditText
        todate = menuLayout.findViewById(R.id.todate) as EditText
        name = menuLayout.findViewById(R.id.name) as EditText
        paymentMode = menuLayout.findViewById(R.id.payment_mode) as AppCompatSpinner

        dg.setView(menuLayout)
        dialog = dg.create()

        val c = Calendar.getInstance()
        val c2 = Calendar.getInstance()

        val btnCancel = menuLayout.findViewById(R.id.cancelbtn) as Button
        val btnClear = menuLayout.findViewById(R.id.clearbtn) as Button
        val btnSet = menuLayout.findViewById(R.id.setbtn) as Button

        setSpinner()

        name.setText(presenter?.viewModel?.filteData?.name)
        fromdate.setText(presenter?.viewModel?.filteData?.from_date)
        todate.setText(presenter?.viewModel?.filteData?.to_Date)

        btnCancel.setOnClickListener {
            dialog?.dismiss()
        }


        btnClear.setOnClickListener {
            fromdate.setText("")
            todate.setText("")
            name.setText("")
            paymentMode.setSelection(0)
            presenter?.viewModel?.filteData = FilterData()
            presenter?.applyFilter()
//            get_all_visitor_history()
            dialog?.dismiss()
        }

        fromdate.setOnClickListener(View.OnClickListener {
            fromDatePickerDialog?.getDatePicker()?.setMaxDate(System.currentTimeMillis())
            fromDatePickerDialog?.show()
        })

        todate.setOnClickListener(View.OnClickListener {
            toDatePickerDialog?.getDatePicker()?.setMaxDate(System.currentTimeMillis())
            toDatePickerDialog?.show()
        })

        btnSet.setOnClickListener {
            try {
                presenter?.applyFilter()

            } catch (ex: ParseException) {
                ex.printStackTrace()
            }
        }

        try {
            fromDatePickerDialog = DatePickerDialog(
                this,
                DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                    try {
                        val newDate = Calendar.getInstance()
                        newDate.set(year, monthOfYear, dayOfMonth)

                        presenter?.saveFromDate(newDate);

                        fromdate.setText(dateFormatter?.format(newDate.time))
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH)
            )

            toDatePickerDialog = DatePickerDialog(
                this,
                DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                    try {
                        val newDate = Calendar.getInstance()
                        newDate.set(year, monthOfYear, dayOfMonth)

                        presenter?.saveToDate(newDate);

                        todate.setText(dateFormatter?.format(newDate.time))
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }, c2.get(Calendar.YEAR), c2.get(Calendar.MONTH), c2.get(Calendar.DAY_OF_MONTH)
            )
        } catch (e: Exception) {
            e.printStackTrace()
        }

        dialog?.show()
    }

    private fun setSpinner() {
        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, companies_list)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        paymentMode.setAdapter(adapter)


        if (presenter?.viewModel?.filteData?.paymentMode != 0) {
            for (i in companies_list.indices) {
                if (PaymentMode.getPaymentMode(companies_list[i]) == (presenter?.viewModel?.filteData?.paymentMode)) {
                    paymentMode.setSelection(i)
                }
            }
        }

        paymentMode.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>, view: View, i: Int, l: Long) {
                presenter?.storePaymentMode(companies_list.get(i))
            }

            override fun onNothingSelected(adapterView: AdapterView<*>) {
            }
        })
    }


    override fun dismissDialog() {
        dialog?.dismiss()
    }

    override fun getTodate(): String {
        return todate.text.toString();
    }

    override fun getFromDate(): String {
        return fromdate.text.toString()
    }

    override fun getName(): String {
        return name.text.toString()
    }

    override fun hideProgressBar() {
        progressBar.visibility = View.GONE
//        recyclerView_customers.visibility = View.VISIBLE
    }

    override fun showProgressBar() {
        progressBar.visibility = View.VISIBLE
//        recyclerView_customers.visibility = View.GONE
    }

    override fun setAdapter(it: CustomerAnalyticResponse, filterData: FilterData) {
        setListAdapter(it, filterData)
    }

    private fun setListAdapter(
        customerListResponse: CustomerAnalyticResponse,
        filterData: FilterData
    ) {
        val viewModel: MainActivityViewModel = this.getViewModel(MainActivityViewModel::class.java)


        this.menuItemsAdapter =
            MainPagedListAdapter(filterData, customerListResponse)

        viewModel.initialLoading?.observe(this, androidx.lifecycle.Observer {
            this.presenter?.handleInitialLoadingNetworkState(it)
        })

        viewModel.networkState.observe(this, androidx.lifecycle.Observer {
            this.menuItemsAdapter?.setNetworkState(it)
        })

        viewModel.menuItemsLiveData.observe(this, androidx.lifecycle.Observer {
            this.menuItemsAdapter?.submitList(it)
        })

        this.layoutManager = LinearLayoutManager(
            this,
            RecyclerView.VERTICAL,
            false
        )

        recyclerView_customers.layoutManager = this.layoutManager
        recyclerView_customers.adapter = this.menuItemsAdapter
    }

    override fun onClickCategory(engineer: String, position: Int) {

        presenter?.storeSelectedPosition(position)

        when (engineer) {
            FilterType.TODAY -> {
                presenter?.setToday()
            }
            FilterType.YESTERDAY -> {
                presenter?.setYesterDay()
            }
            FilterType.THIS_WEEK -> {
                presenter?.setWeek()
            }
            FilterType.CUSTOM -> {
                presenter?.customFilter()
            }
        }
        presenter?.getDetail()
    }


    private fun handleLocationPermissions(): Boolean {
        if (!PermissionUtils.areLocationServicesEnabled(this)) {
            if (!presenter?.locationServiceDialogShowing!!) {       //condition to prevent overlap of location service dialog
                presenter?.locationServiceDialogShowing = true
                locationManager?.showEnableLocationDialog(
                    this,
                    REQUEST_LOCATION_SERVICE
                )
            }
            return false
        } else {
            //Location Services are enabled
            //Permission Disabled
            if (!PermissionUtils.isLocationPermissionAndServiceGranted(this)) {
                if (PermissionUtils.checkIfUserHasDeniedLocationPermissionToNever(
                        deliverrSharedPrefs,
                        this
                    )
                ) {
                    //show SnackBar to open permissions
                    this.snackBarOpenLocationPermissionSettings(
                        this.findViewById(R.id.content_frame),
                        this,
                        R.string.enable_location_permission_deliveryAddress_snackbar_label
                    )

                    showMessage(R.string.enable_location_permission_deliveryAddress_snackbar_label)
                    return false
                }
                if (PermissionUtils.shouldShowLocationPermissionRationale(this)) {
                    //show SnackBar to again popup permissions dialog
                    /*this.snackBarOpenLocationPermissionDialog(this.view!!, activity!!, R.string.enable_location_permission_deliveryAddress_snackbar_label)
*/
                    ActivityCompat.requestPermissions(
                        this,
                        arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                        PermissionRequestCodes.REQUEST_LOCATION
                    )
                    showMessage(R.string.enable_location_permission_deliveryAddress_snackbar_label)
                } else {
                    //asking for permission first time
                    this.snackBarOpenLocationPermissionSettings(
                        this.findViewById(R.id.content_frame),
                        this,
                        R.string.enable_location_permission_deliveryAddress_snackbar_label
                    )
                    presenter?.locationPermissionDialogShowing = true
                    ActivityCompat.requestPermissions(
                        this,
                        arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                        PermissionRequestCodes.REQUEST_LOCATION
                    )
                }

                return false
            } else {
                //Location services enabled
                //Permission enabled
                return true
            }
        }
    }

    private fun snackBarOpenLocationPermissionSettings(
        anchorView: View,
        context: Context,
        stringId: Int
    ) {
        Snackbar.make(anchorView, stringId, Snackbar.LENGTH_INDEFINITE)
            .setAction("Enable") {
                PermissionUtils.startSystemSettings(
                    context,
                    PermissionUtils.LOCATION
                )
            }
            .show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQUEST_LOCATION_SERVICE) {
            this.presenter?.locationServiceDialogShowing = false

            if (resultCode == Activity.RESULT_OK) {
                //location service enabled > check for permission
                LocationActivity.launch(this)
            } else {
                //location service disabled > show snackBar
                if (!PermissionUtils.areLocationServicesEnabled(this)) {
                    showMessage("")
                }
            }
        }
    }

}

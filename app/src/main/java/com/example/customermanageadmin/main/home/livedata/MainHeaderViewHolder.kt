package com.example.customermanageadmin.main.home.livedata

import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.widget.AppCompatTextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.example.customermanageadmin.R
import com.example.customermanageadmin.main.customers.CustomerAnalyticResponse
import com.example.customermanageadmin.models.PaymentMode
import deliverr.deliverrconsumer.utils.roundOffDecimal

class MainHeaderViewHolder(
    rootView: View
) : RecyclerView.ViewHolder(rootView) {
    private val customer: AppCompatTextView = rootView.findViewById(R.id.customer)
    private val amount: AppCompatTextView = rootView.findViewById(R.id.amount)
    private val card_filter: CardView = rootView.findViewById(R.id.card_filter)
    private val view_name_tv: TextView = rootView.findViewById(R.id.view_name_tv)
    private val name_layout: LinearLayout = rootView.findViewById(R.id.name_layout)
    private val from_date_layout: LinearLayout = rootView.findViewById(R.id.from_date_layout)
    private val view_fromdate_tv: TextView = rootView.findViewById(R.id.view_fromdate_tv)
    private val to_date_layout: LinearLayout = rootView.findViewById(R.id.to_date_layout)
    private val view_todate_tv: TextView = rootView.findViewById(R.id.view_todate_tv)
    private val payment_mode_layout: LinearLayout = rootView.findViewById(R.id.payment_mode_layout)
    private val view_payment_mode_tv: TextView = rootView.findViewById(R.id.view_payment_mode_tv)

    fun bind(item: CustomerAnalyticResponse, filterData: FilterData) {

        amount.setText(
            if (item.total_amount == null) {
                "00"
            } else {
                "₹ " + roundOffDecimal(item.total_amount.toFloat())
            }
        )
        customer.setText(
            if (item.total == null) {
                "00"
            } else {
                item.total
            }
        )


        if (filterData.name == "" && filterData.from_date == "" && filterData.to_Date == "" && filterData.paymentMode == 0) {
            card_filter.visibility = View.GONE
        } else {
            card_filter.visibility = View.VISIBLE
            if (filterData.name == "") {
                name_layout.visibility = View.GONE
            } else {
                name_layout.visibility = View.VISIBLE
                view_name_tv.setText(filterData.name)

            }

            if (filterData.from_date == "" || filterData.to_Date == "") {
                from_date_layout.visibility = View.GONE
                to_date_layout.visibility = View.GONE
            } else {
                from_date_layout.visibility = View.VISIBLE
                to_date_layout.visibility = View.VISIBLE
                view_fromdate_tv.setText(filterData.from_date)
                view_todate_tv.setText(filterData.to_Date)
            }

            if (filterData.paymentMode != 0) {
                payment_mode_layout.visibility = View.VISIBLE
                view_payment_mode_tv.setText(PaymentMode.getStringPaymentMode(filterData.paymentMode))
            } else {
                payment_mode_layout.visibility = View.GONE
            }
        }


    }


}

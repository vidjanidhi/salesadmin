package com.example.customermanageadmin.main.home

import com.example.customermanageadmin.application.utils.NetworkState
import com.example.customermanageadmin.application.utils.NetworkStatus
import com.example.customermanageadmin.base.BaseServiceRxWrapper
import com.example.customermanageadmin.main.customers.CustomerAnalyticResponse
import com.example.customermanageadmin.main.customers.livedata.CustomerListResponse
import com.example.customermanageadmin.main.engineer_list.Engineer
import com.example.customermanageadmin.main.engineer_list.EngineerListResponse
import com.example.customermanageadmin.main.login.rx.RetrofitRxWrapper
import com.example.customermanageadmin.main.register.rx.AddEngineerResponse
import com.example.customermanageadmin.models.PaymentMode
import com.example.customermanageadmin.realm_model.CustomerRepository
import com.example.customermanageadmin.realm_model.EngineerRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import java.net.UnknownHostException
import java.text.SimpleDateFormat
import java.util.*

class MainActivityPresenter(
    private val delegate: MainActivityViewDelegate,
    private val wrapper: RetrofitRxWrapper,
    private val customerRepository: CustomerRepository,
    private val engineerRepository: EngineerRepository,
    private val isNetworkAvailable: Boolean
) {
    private var disposable: Disposable? = null
    internal var locationServiceDialogShowing: Boolean = false
    internal var locationPermissionDialogShowing: Boolean = false
    val sdf = SimpleDateFormat("yyyy-MM-dd")

    val inputFormat = SimpleDateFormat("dd-MM-yyyy")
    val outputFormat = SimpleDateFormat("yyyy-MM-dd")
    val viewModel: MainActivityViewModel by lazy {
        this.delegate.getViewModel(MainActivityViewModel::class.java)
    }


    fun applyFilter() {

        if (delegate.getFromDate() != "" && delegate.getTodate() != "") {
            val _to_date = inputFormat.parse("" + delegate.getTodate())
            val _from_date = inputFormat.parse("" + delegate.getFromDate())

            val to_date = sdf.parse("" + outputFormat.format(_to_date))  //to followup_date
            val frdate = sdf.parse("" + outputFormat.format(_from_date)) // from followup_date

            if (frdate.after(to_date)) {
                delegate.showMessage("Fromdate Should Be Less then todate")

            } else if (to_date.before(frdate)) {
                delegate.showMessage("Todate  Should Be More then Fromdate")

            } else {
                filterData()
            }
        } else {
            filterData()
        }


    }

    fun filterData() {
        viewModel.filteData?.name = delegate.getName()
        getDetail()
        delegate.dismissDialog()
        /*if (delegate.isConnectedToInternet()) {



        } else {
            delegate.showMessage("No Internet")
            delegate.dismissDialog()
        }*/
    }

    fun handleInitialLoadingNetworkState(networkState: NetworkState) {
        when (networkState.status) {
            NetworkStatus.RUNNING -> this.delegate.showProgressBar()
            NetworkStatus.SUCCESS -> {
                this.delegate.hideProgressBar()
            }
            NetworkStatus.FAILED -> {
                this.delegate.hideProgressBar()
                if (networkState.message.isNotBlank()) {
                    this.delegate.showMessage(networkState.message)
                } else {
                    this.delegate.showMessage(com.example.customermanageadmin.R.string.error_email)
                }
            }
        }
    }

    fun getItems() {
        viewModel.filteData?.let {
            viewModel.initItems(
                it,
                wrapper,
                delegate.isConnectedToInternet()
            )
        }
        viewModel.customerListResponse?.let {
            viewModel.filteData?.let { it1 ->
                delegate.setAdapter(
                    it,
                    it1
                )
            }
        }

    }

    fun getDetail() {
        if (delegate.isConnectedToInternet()) {
            delegate.showProgressBar()
            getAnalyticsDetails()
        } else {
            getAnalyticsDetailsFromRealm()
            delegate.hideProgressBar()
//            delegate.showMessage(com.example.customermanageadmin.R.string.no_internet_connection_message)
        }
    }

    private fun getAnalyticsDetailsFromRealm() {

        var customerAnalyticResponse = CustomerAnalyticResponse()
        customerAnalyticResponse.total =
            viewModel?.filteData?.let { customerRepository.countTotalItemsFilter(it).toString() }
        customerAnalyticResponse.total_amount =
            viewModel?.filteData?.let { customerRepository.countTotalAmountFilter(it).toString() }
        viewModel.customerListResponse = customerAnalyticResponse
        success()
    }

    fun getAnalyticsDetails() {
        delegate.hideProgressBar()
        if (viewModel.customerListSingle == null) {
            viewModel.customerListSingle =
                viewModel.filteData?.let {
                    wrapper.getBrandItemListDetail(
                        it
                    )
                }
        }
        disposable?.dispose()

        disposable = this.viewModel.customerListSingle?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribeWith(object : DisposableSingleObserver<CustomerAnalyticResponse>() {
                override fun onSuccess(customerListResponse: CustomerAnalyticResponse) {

                    viewModel.customerListResponse = customerListResponse
                    viewModel.customerListSingle = null
                    if (customerListResponse.isSuccess == true) {
                        success()
                        delegate.dismissDialog()
                    } else {
                        success()
                        delegate.dismissDialog()
                        customerListResponse.message?.let { failed(it) }
                    }
                }

                override fun onError(e: Throwable) {
                    viewModel.customerListSingle = null
                    var message: String
                    if (e is UnknownHostException) {
                        message = BaseServiceRxWrapper.NO_INTERNET_CONNECTION
                    } else {
                        message = e.message.toString()
                    }
                    failed(message)
                    dispose()
                }
            })
    }

    fun success() {
        getItems()
        viewModel.customerListResponse?.let {
            viewModel.filteData?.let { it1 ->
                delegate.setAdapter(
                    it,
                    it1
                )
            }

        }
    }

    fun failed(msg: String) {
        delegate.showMessage(msg)
    }

    fun saveToDate(newDate: Calendar) {
        viewModel.toDate = newDate
        viewModel.filteData?.to_Date = sdf.format(newDate.time)
        viewModel.filteData?.toDate = Date(newDate.getTimeInMillis())
    }

    fun saveFromDate(newDate: Calendar) {
        viewModel.fromDate = newDate
        viewModel.filteData?.from_date = sdf.format(newDate.time)
        viewModel.filteData?.fromDate = Date(newDate.getTimeInMillis())
    }


    fun storePaymentMode(i: String) {
        viewModel.paymentMode = PaymentMode.getPaymentMode(i)
        viewModel.filteData?.paymentMode = viewModel.paymentMode
    }

    fun setToday() {
        val calendar = Calendar.getInstance()
        calendar.set(Calendar.HOUR_OF_DAY, 0)
        calendar.set(Calendar.MINUTE, 0)
        saveFromDate(calendar)


        calendar.set(Calendar.HOUR_OF_DAY,23)
        calendar.set(Calendar.MINUTE, 59)

        saveToDate(calendar)
    }

    fun setYesterDay() {
        val calendar = Calendar.getInstance()
        calendar.add(Calendar.DATE, -1)
        calendar.set(Calendar.HOUR_OF_DAY, 0)
        calendar.set(Calendar.MINUTE, 0)
        saveFromDate(calendar)

        calendar.set(Calendar.HOUR_OF_DAY, 23)
        calendar.set(Calendar.MINUTE, 59)

        saveToDate(calendar)
    }

    fun setWeek() {
        val calendar = Calendar.getInstance()
        calendar.add(Calendar.DATE, -7);
        saveFromDate(calendar)
        saveToDate(Calendar.getInstance())
    }

    fun customFilter() {
        removeName()
        removePaymentMode()
        removeFromDate()
        removeToDate()
    }

    fun removeFromDate() {
        viewModel.filteData?.to_Date = ""
        viewModel.filteData?.toDate = Date()
    }

    fun removeToDate() {
        viewModel.filteData?.from_date = ""
        viewModel.filteData?.fromDate = Date()
    }

    fun removeName() {
        viewModel.filteData?.name = ""
    }

    fun removePaymentMode() {
        viewModel.filteData?.paymentMode = 0
    }

    fun storeSelectedPosition(position: Int) {
        viewModel.selectedPosition = position
        delegate.notifyAdapter()
    }

    fun getSelectedPosition(): Int {
        return viewModel.selectedPosition
    }

    fun sync() {
        getCustomerList()
    }

    fun getCustomerList() {
        if (viewModel.customerListSingle1 == null) {
            viewModel.customerListSingle1 =
                wrapper.getBrandItemListCall()
        }
        disposable?.dispose()

        disposable = this.viewModel.customerListSingle1?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribeWith(object : DisposableSingleObserver<CustomerListResponse>() {
                override fun onSuccess(customerListResponse: CustomerListResponse) {

                    viewModel.customerListResponse1 = customerListResponse
                    viewModel.customerListSingle1 = null
                    if (customerListResponse.isSuccess == true) {
                        storeToLocal()
                    } else {
                        customerListResponse.message?.let { }
                    }
                }

                override fun onError(e: Throwable) {
                    viewModel.customerListSingle1 = null
                    dispose()
                }
            })
    }

    fun storeToLocal() {
        customerRepository.deleteAllItems()
        var count = customerRepository.countTotalItems()
        viewModel.customerListResponse1?.payload?.let { customerRepository.insertOrUpdate(it) }
        var count1 = customerRepository.countTotalItems()
        sendAddedEngineers()

//        delegate.openNext()
    }

    private fun sendAddedEngineers() {
        var engineers = engineerRepository.findAdds()
        if (engineers.size > 0) {
            addEngineerWrapper(engineers[0])
        } else {
            sendEditedEngineers()
        }

    }

    fun addEngineerWrapper(engineer: Engineer) {

        if (viewModel.addResponseSingle == null) {
            viewModel.addResponseSingle =
                wrapper.addEngineer(
                    engineer.getName(),
                    engineer.getEmail(),
                    engineer.getPhone(),
                    engineer.getAddress(),
                    engineer.getPassword()
                )
        }
        disposable?.dispose()

        disposable = this.viewModel.addResponseSingle?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribeWith(object : DisposableSingleObserver<AddEngineerResponse>() {
                override fun onSuccess(loginResponse: AddEngineerResponse) {

                    viewModel.addResponse = loginResponse
                    viewModel.addResponseSingle = null
                    if (loginResponse.isSuccess == true) {
                        deleteAddedEngineer(engineer.email)
                        sendAddedEngineers()
                    } else {
                        deleteAddedEngineer(engineer.email)
                        sendAddedEngineers()
                    }
                }

                override fun onError(e: Throwable) {
                    viewModel.addResponseSingle = null
                    dispose()
                }
            })
    }

    private fun deleteAddedEngineer(email: String) {
        engineerRepository.deleteAddedItem(email)
    }

    private fun sendEditedEngineers() {
        var engineers = engineerRepository.findEdits()
        if (engineers.size > 0) {
            editEngineerWrapper(engineers[0])
        } else {
            getEngineers()
        }
    }

    fun editEngineerWrapper(engineer: Engineer) {

        if (viewModel.editResponseSingle == null) {
            viewModel.editResponseSingle =
                wrapper.editEngineer(
                    engineer?.id,
                    engineer.getName(),
                    engineer.getEmail(),
                    engineer.getPhone(),
                    engineer.getAddress()
                )
        }
        disposable?.dispose()

        disposable = this.viewModel.editResponseSingle?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribeWith(object : DisposableSingleObserver<AddEngineerResponse>() {
                override fun onSuccess(editResponse: AddEngineerResponse) {

                    viewModel.editResponse = editResponse
                    viewModel.editResponseSingle = null
                    if (editResponse.isSuccess == true) {
                        deleteEditedEngineer(engineer.id)
                    } else {
                    }
                }

                override fun onError(e: Throwable) {
                    viewModel.editResponseSingle = null

                    dispose()
                }
            })
    }

    private fun deleteEditedEngineer(id: String) {
        engineerRepository.deleteEditedItem(id)
        sendEditedEngineers()
    }


    fun getEngineers() {

        if (viewModel.engineerResponseSingle == null) {
            viewModel.engineerResponseSingle =
                wrapper.getEngineers()
        }
        disposable?.dispose()

        disposable = this.viewModel.engineerResponseSingle?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribeWith(object : DisposableSingleObserver<EngineerListResponse>() {
                override fun onSuccess(loginResponse: EngineerListResponse) {

                    viewModel.engineerListResponse = loginResponse
                    viewModel.engineerResponseSingle = null
                    if (loginResponse.isSuccess == true) {
                        success1()

                    } else {

                    }
                }

                override fun onError(e: Throwable) {
                    viewModel.engineerResponseSingle = null

                    dispose()
                }
            })
    }

    private fun success1() {
        engineerRepository.deleteAllGetItems()
        var count = engineerRepository.countTotalItems()
        viewModel.engineerListResponse?.payload?.let { engineerRepository.insertOrUpdate(it) }
        var count1 = engineerRepository.countTotalItems()
        delegate.hideProgressBar()
        getDetail()
    }
}

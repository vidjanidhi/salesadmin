package com.example.customermanageadmin.main.login.rx

import com.example.customermanageadmin.base.BaseServiceRxWrapper
import com.example.customermanageadmin.base.RetrofitService
import com.example.customermanageadmin.main.customers.CustomerAnalyticResponse
import com.example.customermanageadmin.main.customers.livedata.CustomerListResponse
import com.example.customermanageadmin.main.engineer_list.EngineerListResponse
import com.example.customermanageadmin.main.home.livedata.FilterData
import com.example.customermanageadmin.main.location.LocationListResponse
import com.example.customermanageadmin.main.register.rx.AddEngineerResponse
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.Call

class RetrofitRxWrapper : BaseServiceRxWrapper() {
    private val service: RetrofitService = this.retrofit.create(RetrofitService::class.java)

    fun adminLogin(
        email: String,
        password: String

    ): Single<AdminLoginResponse> {
        return this.service.adminLogin(email, password)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .cache()
    }

    fun addEngineer(
        name: String,
        email: String,
        phone: String,
        address: String,
        password: String

    ): Single<AddEngineerResponse> {
        return this.service.addEngineer(name, email, phone, address, password)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .cache()
    }

    fun editEngineer(
        id: String,
        name: String,
        email: String,
        phone: String,
        address: String

    ): Single<AddEngineerResponse> {
        return this.service.editEngineer(id, name, email, phone, address)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .cache()
    }

    fun getBrandItemListCall(
        skip: Int,
        take: Int
    ): Call<CustomerListResponse> {
        return this.service.getBrandItemList(
            skip,
            take
        )
    }

    fun getBrandItemListCall(

    ): Single<CustomerListResponse> {
        return this.service.getBrandItemList(

        )
    }

    fun getBrandItemListByLocation(
        lat:String,
        lng:String,
        distance: Int
    ): Single<LocationListResponse> {
        return this.service.getBrandItemListByLocation(
            lat,lng,
            distance
        )
    }

    fun getMaxDistance(
        lat: String, lng: String

    ): Single<LocationListResponse> {
        return this.service.getMaxDistance(
            lat, lng
        )
    }

    fun getBrandItemListCall(
        filterData: FilterData,
        skip: Int,
        take: Int
    ): Call<CustomerListResponse> {
        return this.service.getBrandItemList(
            filterData.name,
            "custom",
            filterData.from_date,
            filterData.to_Date,
            getPay_mode(filterData.paymentMode),
            skip,
            take
        )
    }

    fun getBrandItemListDetail(
        filterData: FilterData
    ): Single<CustomerAnalyticResponse> {
        return this.service.getBrandItemListDetail(
            filterData.name,
            "custom",
            filterData.from_date,
            filterData.to_Date,
            getPay_mode(filterData.paymentMode)
        )
    }

    fun getBrandItemListDetail(): Single<CustomerAnalyticResponse> {
        return this.service.getBrandItemListDetail()
    }

    fun getPay_mode(payMode: Int): String {
        return if (payMode != 0) {
            payMode.toString()
        } else {
            ""
        }
    }

    fun getEngineers(): Single<EngineerListResponse> {
        return this.service.getEngineers()
    }

}

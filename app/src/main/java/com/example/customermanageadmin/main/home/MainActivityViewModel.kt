package com.example.customermanageadmin.main.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.example.customermanageadmin.application.utils.NetworkState
import com.example.customermanageadmin.main.customers.CustomerAnalyticResponse
import com.example.customermanageadmin.main.customers.livedata.Customer
import com.example.customermanageadmin.main.customers.livedata.CustomerListResponse
import com.example.customermanageadmin.main.engineer_list.EngineerListResponse
import com.example.customermanageadmin.main.home.livedata.FilterData
import com.example.customermanageadmin.main.login.rx.RetrofitRxWrapper
import com.example.customermanageadmin.main.register.rx.AddEngineerResponse
import com.example.customermanageadmin.realm_model.CustomerRealmRepository
import com.example.customermanageadmin.realm_model.CustomerRepository
import deliverr.deliverrconsumer.screens.add_item.store_categories_grid_view.brand_items_screen.FUTURE_PAGE_SIZE
import deliverr.deliverrconsumer.screens.add_item.store_categories_grid_view.brand_items_screen.INITIAL_PAGE_SIZE
import deliverr.deliverrconsumer.screens.add_item.store_categories_grid_view.brand_items_screen.MainDataSource
import deliverr.deliverrconsumer.screens.add_item.store_categories_grid_view.brand_items_screen.MainDataSourceFactory
import io.reactivex.Single
import java.util.*

class MainActivityViewModel : ViewModel() {


    var customerListSingle1: Single<CustomerListResponse>? = null
    var customerListResponse1: CustomerListResponse?=null

    var engineerResponseSingle: Single<EngineerListResponse>? = null
    var engineerListResponse: EngineerListResponse?=null

    var editResponseSingle: Single<AddEngineerResponse>? = null
    var editResponse: AddEngineerResponse?=null

    var addResponseSingle: Single<AddEngineerResponse>? = null
    var addResponse: AddEngineerResponse?=null

    var selectedPosition: Int=0
    var filteData= FilterData()
    var paymentMode: Int = 0

    var fromDate: Calendar = Calendar.getInstance()
    var toDate: Calendar = Calendar.getInstance()
    var customerListSingle: Single<CustomerAnalyticResponse>? = null
    var customerListResponse: CustomerAnalyticResponse? = null

     var initialLoading: LiveData<NetworkState>? = null
    lateinit var networkState: LiveData<NetworkState>
    lateinit var menuItemsLiveData: LiveData<PagedList<Customer>>

    lateinit var mainDataSource: LiveData<MainDataSource>

    fun retry() {
        this.mainDataSource.value!!.retryLoading()
    }

    fun initItems(
        filterData: FilterData,
        wrapper: RetrofitRxWrapper,
        isNetworkAvailable: Boolean
    ) {
        val brandItemsDataSourceFactory = MainDataSourceFactory(
            filterData,
            wrapper,isNetworkAvailable
        )
        this.mainDataSource = brandItemsDataSourceFactory.mainDataSourceLiveData

        this.initialLoading = Transformations.switchMap(
            brandItemsDataSourceFactory.mainDataSourceLiveData
        ) { it.initialLoading }
        this.networkState = Transformations.switchMap(
            brandItemsDataSourceFactory.mainDataSourceLiveData
        ) { it.networkState }

        val pagedListConfig = PagedList.Config.Builder()
            .setEnablePlaceholders(false)
            .setInitialLoadSizeHint(INITIAL_PAGE_SIZE)
            .setPageSize(FUTURE_PAGE_SIZE)
            .setPrefetchDistance(FUTURE_PAGE_SIZE)
            .build()

        this.menuItemsLiveData = LivePagedListBuilder(brandItemsDataSourceFactory, pagedListConfig)
            .build()
    }

}

package com.example.customermanageadmin.main.customers.livedata

import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.text.util.Linkify
import android.view.View
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.customermanageadmin.R
import com.example.customermanageadmin.models.PaymentMode
import deliverr.deliverrconsumer.utils.roundOffDecimal

class CustomerViewHolder(
    rootView: View
) : RecyclerView.ViewHolder(rootView) {
    private val name: AppCompatTextView = rootView.findViewById(R.id.title)
    private val email: AppCompatTextView = rootView.findViewById(R.id.email)
    private val phone: AppCompatTextView = rootView.findViewById(R.id.phone)
    private val payment_mode: AppCompatTextView = rootView.findViewById(R.id.payment_mode)
    private val amount: AppCompatTextView = rootView.findViewById(R.id.amount)

    fun bind(item: Customer) {
        name.setText(item.name)
        email.setText("Email : " + item.email)
        Linkify.addLinks(email, Linkify.EMAIL_ADDRESSES)
        phone.setText("Phone : " + item.phone)
        Linkify.addLinks(phone, Linkify.PHONE_NUMBERS)
        payment_mode.setText("Payment Mode : " + PaymentMode.getStringPaymentMode(item.payment_mode.toInt()))
        amount.setText(
            "₹ " + roundOffDecimal(item.amount)
        )
    }
}

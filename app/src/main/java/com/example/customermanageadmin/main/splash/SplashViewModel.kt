package com.example.customermanageadmin.main.splash

import androidx.lifecycle.ViewModel
import com.example.customermanageadmin.main.customers.livedata.CustomerListResponse
import com.example.customermanageadmin.main.engineer_list.EngineerListResponse
import com.example.customermanageadmin.main.login.rx.AdminLoginResponse
import com.example.customermanageadmin.main.register.rx.AddEngineerResponse
import io.reactivex.Single

class SplashViewModel : ViewModel() {

    var customerListSingle: Single<CustomerListResponse>? = null
    var customerListResponse: CustomerListResponse ?=null

    var engineerResponseSingle: Single<EngineerListResponse>? = null
    var engineerListResponse: EngineerListResponse?=null

    var editResponseSingle: Single<AddEngineerResponse>? = null
    var editResponse: AddEngineerResponse?=null

    var addResponseSingle: Single<AddEngineerResponse>? = null
    var addResponse: AddEngineerResponse ?=null
}

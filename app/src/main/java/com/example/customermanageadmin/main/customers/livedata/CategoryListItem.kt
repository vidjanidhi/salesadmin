package com.example.customermanageadmin.main.customers.livedata

import androidx.recyclerview.widget.DiffUtil

data class CategoryListItem(
    val itemType: CategoryListItemType,
    val customerListrReaponse: CustomerListResponse? = null,
    val customer: Customer? = null
) {
    companion object {
        val DIFF_CALLBACK: DiffUtil.ItemCallback<CategoryListItem> =
            object : DiffUtil.ItemCallback<CategoryListItem>() {
                override fun areItemsTheSame(
                    oldItem: CategoryListItem,
                    newItem: CategoryListItem
                ): Boolean {
                    return (oldItem.itemType == newItem.itemType) &&
                            (oldItem.customerListrReaponse == newItem.customerListrReaponse) &&
                            (oldItem.customer == newItem.customer)
                }

                override fun areContentsTheSame(
                    oldItem: CategoryListItem,
                    newItem: CategoryListItem
                ): Boolean {
                    return this.areItemsTheSame(oldItem, newItem)
                }
            }
    }
}

enum class CategoryListItemType {

    ITEM_TYPE_CUSTOMER,
    ITEM_TYPE_CUSTOMER_HEADER

}

fun createCategoryListItemList(
    customerListrReaponse: CustomerListResponse
): List<CategoryListItem> {
    val outputList: MutableList<CategoryListItem> = mutableListOf()

    outputList.add(
        CategoryListItem(
            itemType = CategoryListItemType.ITEM_TYPE_CUSTOMER_HEADER,
            customerListrReaponse = customerListrReaponse
        )
    )

    outputList.addAll(customerListrReaponse.payload.map {
        CategoryListItem(
            itemType = CategoryListItemType.ITEM_TYPE_CUSTOMER,
            customer = it
        )
    })

    return outputList
}

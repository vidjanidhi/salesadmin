package com.example.customermanageadmin.base

data class ShowCommonDialogEvent(
        val dialogMessage: String,
        val dialogTitle: String,
        val buttonAction: String
)

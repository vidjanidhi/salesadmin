package com.example.customermanageadmin.base

import com.example.customermanageadmin.main.Constants
import com.example.customermanageadmin.main.customers.CustomerAnalyticResponse
import com.example.customermanageadmin.main.customers.livedata.CustomerListResponse
import com.example.customermanageadmin.main.engineer_list.EngineerListResponse
import com.example.customermanageadmin.main.location.LocationListResponse
import com.example.customermanageadmin.main.login.rx.AdminLoginResponse
import com.example.customermanageadmin.main.register.rx.AddEngineerResponse
import io.reactivex.Single
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

internal interface RetrofitService {
    @GET(Constants.Admin.LOGIN)
    fun adminLogin(
        @Query("email") email: String,
        @Query("password") password: String
    ): Single<AdminLoginResponse>

    @GET(Constants.Admin.ADD_ENGINEER)
    fun addEngineer(
        @Query("name") name: String,
        @Query("email") email: String,
        @Query("phone") phone: String,
        @Query("address") address: String,
        @Query("password") password: String
    ): Single<AddEngineerResponse>

    @GET(Constants.Admin.UPDATE_ENGINEER)
    fun editEngineer(
        @Query("id") id: String,
        @Query("name") name: String,
        @Query("email") email: String,
        @Query("phone") phone: String,
        @Query("address") address: String
    ): Single<AddEngineerResponse>

    @GET(Constants.Admin.GET_CUSTOMERS)
    fun getBrandItemList(
        @Query("ul") skip: Int,
        @Query("ll") take: Int
    ): Call<CustomerListResponse>

    @GET(Constants.Admin.GET_CUSTOMERS)
    fun getBrandItemList(
    ): Single<CustomerListResponse>

    @GET(Constants.Admin.GET_CUSTOMERS_BY_DISTANCE)
    fun getBrandItemListByLocation(
        @Query("lat") lat: String,
        @Query("lng") lng: String,
        @Query("distance") distance: Int
    ): Single<LocationListResponse>

    @GET(Constants.Admin.GET_MAX_DISTANCE)
    fun getMaxDistance(
        @Query("lat") lat: String,
        @Query("lng") lng: String
    ): Single<LocationListResponse>

    @GET(Constants.Admin.GET_CUSTOMERS)
    fun getBrandItemList(
        @Query("name") name: String,
        @Query("time") time: String,
        @Query("from_date") from_date: String,
        @Query("to_date") to_date: String,
        @Query("pay_mode") payment_mode: String,
        @Query("ul") skip: Int,
        @Query("ll") take: Int
    ): Call<CustomerListResponse>

    @GET(Constants.Admin.GET_CUSTOMERS_DETAIL)
    fun getBrandItemListDetail(

    ): Single<CustomerAnalyticResponse>

    @GET(Constants.Admin.GET_ENGINEERS)
    fun getEngineers(

    ): Single<EngineerListResponse>

    @GET(Constants.Admin.GET_CUSTOMERS_DETAIL)
    fun getBrandItemListDetail(
        @Query("name") name: String,
        @Query("time") time: String,
        @Query("from_date") from_date: String,
        @Query("to_date") to_date: String,
        @Query("pay_mode") payment_mode: String
    ): Single<CustomerAnalyticResponse>

}

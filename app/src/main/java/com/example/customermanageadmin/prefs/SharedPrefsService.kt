package com.example.customermanageadmin.prefs

interface SharedPrefsService {
    var isLogin: Boolean

    var adminName: String
    var adminId: String

    var isExternalStoragePermissionAsked: Boolean

    var isCameraPermissionAsked: Boolean

    var isLocationPermissionAsked: Boolean

    fun shouldReinit(): Boolean

    fun setIsAlcoholAgreed(value: Boolean)

    fun clearPreferences(): Boolean


}

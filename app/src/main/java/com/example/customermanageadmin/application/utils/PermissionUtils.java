package com.example.customermanageadmin.application.utils;

import android.Manifest;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import com.example.customermanageadmin.di.location.LocationManager;
import com.example.customermanageadmin.prefs.SharedPrefs;
import com.example.customermanageadmin.prefs.SharedPrefsService;
import com.google.android.material.snackbar.Snackbar;


public class PermissionUtils {
    public static final String CAMERA = "CAMERA";
    public static final String STORAGE = "STORAGE";
    public static final String LOCATION = "LOCATION";

    private PermissionUtils() {
        throw new UnsupportedOperationException();
    }

    public static boolean isReadExternalStorageGranted(final Activity activity) {
        return ActivityCompat.checkSelfPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE)
                ==
                PackageManager.PERMISSION_GRANTED;
    }

    public static boolean isWriteExternalStorageGranted(final Activity activity) {
        return ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                ==
                PackageManager.PERMISSION_GRANTED;
    }

    public static boolean shouldShowReadExternalStoragePermissionRationale(final Activity activity) {
        return ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.READ_EXTERNAL_STORAGE);
    }

    public static void requestWriteExternalStoragePermission(Activity activity) {
        ActivityCompat.
                requestPermissions(activity,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        PermissionRequestCodes.INSTANCE.getREQUEST_WRITE_STORAGE());
    }

    public static boolean handleLocationPermissions(
            final Activity activity,
            Fragment fragment,
            SharedPrefs deliverrSharedPrefs,
            LocationManager locationManager,
            int requestCode
    ) {
        if (activity == null) {
            return false;
        } else if (isLocationPermissionAndServiceGranted(activity)) {
            return true;
        } else if (!isLocationPermissionAndServiceGranted(activity)) {
            if (!areLocationServicesEnabled(activity)) {
                showEnableLocationServicesDialog(activity, locationManager, requestCode);
                return false;
            } else {
                if (shouldShowLocationPermissionRationale(activity)) {
                    showLocationRationaleDialog(activity, fragment, deliverrSharedPrefs);
                    return false;
                }
            }
        }
        if (fragment != null) {
            fragment.requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    PermissionRequestCodes.INSTANCE.getREQUEST_LOCATION());
            return false;
        }
        ActivityCompat.
                requestPermissions(activity,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        PermissionRequestCodes.INSTANCE.getREQUEST_LOCATION());
        return false;
    }

    public static boolean isLocationPermissionAndServiceGranted(Context context) {
        if (context == null) {
            return false;
        }
        return ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && areLocationServicesEnabled(context);
    }

    private static void showEnableLocationServicesDialog(final Activity activity, final LocationManager locationManager, final int requestCode) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity)
                .setMessage("Location Services Are Not Enabled Do you want to enable it now")
                .setPositiveButton("Enable", (dialog, which) -> {
                    if (locationManager != null && requestCode != 0) {
                        locationManager.showEnableLocationDialog(activity, requestCode);
                    }

                })
                .setNegativeButton("Cancel", (dialogInterface, i) -> dialogInterface.dismiss());
        builder.show();
    }

    public static boolean areLocationServicesEnabled(Context context) {
        String locationsAllowed = Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.LOCATION_PROVIDERS_ALLOWED);

        return !TextUtils.isEmpty(locationsAllowed);
    }


    public static boolean shouldShowLocationPermissionRationale(Activity activity) {
        // Guard Clause
        if (activity == null) {
            return false;
        }

        return ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.ACCESS_FINE_LOCATION);
    }

    public static boolean isCameraPermissionGranted(Context context) {
        // Guard Clause
        if (context == null) {
            return false;
        }

        return ActivityCompat.checkSelfPermission(context, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;
    }

    private static void showLocationRationaleDialog(final Activity activity, final Fragment fragment, final SharedPrefs dm) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity)
                .setMessage("Please allow location permissions in order to search nearby places")
                .setPositiveButton("Allow", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (dm != null) {
                            dm.setLocationPermissionAsked(true);
                        }
                        if (fragment != null) {
                            fragment.
                                    requestPermissions(
                                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                            PermissionRequestCodes.INSTANCE.getREQUEST_LOCATION());
                            return;
                        }
                        ActivityCompat.
                                requestPermissions(activity,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        PermissionRequestCodes.INSTANCE.getREQUEST_LOCATION());
                    }
                });
        builder.show();
    }


    public static boolean checkIfUserHasDeniedStoragePermissionToNever(SharedPrefs dm, Activity activity) {
        if (dm != null) {
            return dm.isExternalStoragePermissionAsked() &&
                    !shouldShowReadExternalStoragePermissionRationale(activity);
        }
        return false;
    }

    public static boolean checkIfUserHasDeniedCameraPermissionToNever(SharedPrefs dm, Activity activity) {
        if (dm != null) {
            return dm.isCameraPermissionAsked() &&
                    !shouldShowCameraPermissionRationale(activity);
        }
        return false;
    }

    public static boolean shouldShowCameraPermissionRationale(Activity activity) {
        // Guard Clause
        if (activity == null) {
            return false;
        }

        return ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.CAMERA);
    }

    public static boolean checkIfUserHasDeniedLocationPermissionToNever(SharedPrefsService dm, Activity activity) {
        if (dm != null) {
            return dm.isLocationPermissionAsked() &&
                    !shouldShowLocationPermissionRationale(activity);
        }
        return false;
    }

    public static void startSystemSettings(Context context, String source) {
        Intent myAppSettings = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                Uri.parse("package:" + context.getPackageName()));
        myAppSettings.addCategory(Intent.CATEGORY_DEFAULT);
        myAppSettings.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        try {
            context.startActivity(myAppSettings);
            if (source.equals(CAMERA)) {
                Toast.makeText(context,
                        "Tap on Permissions then enable Camera Permissions", Toast.LENGTH_LONG).show();
            } else if (source.equals(STORAGE)) {
                Toast.makeText(context,
                        "Tap on Permissions then enable Storage Permissions", Toast.LENGTH_LONG).show();
            } else if (source.equals(LOCATION)) {
                Toast.makeText(context,
                        "Tap on Permissions then enable Location Permissions", Toast.LENGTH_LONG).show();
            }

        } catch (ActivityNotFoundException e) {
            Toast.makeText(context,
                    "There is something wrong with your device settings. Please go to the system settings screen and enable permissions.", Toast.LENGTH_LONG).show();
        }
    }

    public static void openSystemSettingsLocationScreen(Context context) {
        if (!areLocationServicesEnabled(context)) {
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            context.startActivity(intent);
        } else {
            // Can happen when the user disable the gps from system bar
            //updateLocationRowStatus();
        }
    }

    public static void showLocationDeniedSnackBar(View anchorView, final Context context, int stringId) {
        Snackbar.make(anchorView, stringId, Snackbar.LENGTH_INDEFINITE)
                .setAction("Enable", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        PermissionUtils.startSystemSettings(context, PermissionUtils.LOCATION);
                    }
                })
                .show();
    }
}

@file:JvmName("VerifyUtils")

package deliverr.deliverrconsumer.utils

import android.util.Patterns
import java.math.RoundingMode
import java.text.DecimalFormat

fun isValidEmail(target: CharSequence?): Boolean {
    return target != null && Patterns.EMAIL_ADDRESS.matcher(target).matches()
}
fun roundOffDecimal(number: Float): Float? {
    val df = DecimalFormat("#.##")
    df.roundingMode = RoundingMode.CEILING
    return df.format(number).toFloat()
}
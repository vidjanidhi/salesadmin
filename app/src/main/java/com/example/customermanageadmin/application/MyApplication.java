package com.example.customermanageadmin.application;

import android.content.Context;
import android.location.LocationManager;
import android.view.View;

import androidx.multidex.MultiDex;

import com.bluelinelabs.conductor.Controller;
import com.example.customermanageadmin.di.components.DaggerDeliverrApplicationComponent;
import com.example.customermanageadmin.di.conductor.HasConductorInjector;
import com.example.customermanageadmin.di.views.HasViewInjector;
import com.example.customermanageadmin.prefs.SharedPrefsService;

import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;
import dagger.android.DispatchingAndroidInjector;
import io.realm.Realm;

public class MyApplication extends DaggerApplication implements HasConductorInjector, HasViewInjector {
    public static final String TAG = MyApplication.class.getSimpleName();

    @Inject
    public SharedPrefsService deliverrSharedPrefs;

    @Inject
    public Realm realm;
    /*@Inject
    ConsumerInfoRealmRepository consumerInfoRealmRepository;*/

//    @Inject
//    LocationManagerLifecycleHandler locationManagerLifecycleHandler;
    @Inject
    DispatchingAndroidInjector<Controller> controllerInjector;
    @Inject
    DispatchingAndroidInjector<View> viewInjector;


    @Override
    public void onCreate() {
        super.onCreate();

//        this.registerLifeCycleCallbacks();
    }

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        return DaggerDeliverrApplicationComponent.builder().application(this).build();
    }


  /*  private void registerLifeCycleCallbacks() {
        registerActivityLifecycleCallbacks(locationManagerLifecycleHandler);
    }*/

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }


    public SharedPrefsService getDeliverrSharedPrefs() {
        return deliverrSharedPrefs;
    }




    public Realm getRealm() {
        if (realm != null && realm.isClosed()) {
            return Realm.getDefaultInstance();
        }
        return realm;
    }

    @NotNull
    @Override
    public DispatchingAndroidInjector<Controller> controllerInjector() {
        return controllerInjector;
    }

    @NotNull
    @Override
    public AndroidInjector<View> viewInjector() {
        return viewInjector;
    }

//    private void registerLifeCycleCallbacks() {
//        registerActivityLifecycleCallbacks(locationManagerLifecycleHandler);
//    }

}
